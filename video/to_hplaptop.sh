#! /bin/sh

set +x

HP_LAPTOP_IP=192.168.15.6
DEST_DIR=`pwd`/temp
DEST_COMP=hilton@"${HP_LAPTOP_IP}":"${DEST_DIR}"
echo DEST_COMP="${DEST_COMP}"

# TODO rename previous or current videoi with the same name

if [ $# -lt 1 ] ; then
    echo $0": ERROR file name to copy was not informed"
    exit 1
fi

INP_NAME=$1

# TODO test if there is an input file with the given name

scp "${INP_NAME}" "${DEST_COMP}"
