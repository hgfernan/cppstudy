#include <iostream>   // cout, endl

// Confirming disallowment of general rule in capture list

template <typename T>
    void
    tell_change(const char* title, T current, T previous) {
        std::cout << title << " ";
        if (current == previous) {
            std::cout << "didn't change";
        } else {
            std::cout << "changed from " 
                      << previous << " to " << current;
        }
        
        std::cout << ", ";
    }

template <typename T> 
    void
    restore(T& current, T& previous) {
        current = previous;
    }

int main() 
{
    int    a, a_sav;
    double b, b_sav;
    float  c, c_sav;
    
    a = a_sav = 1;
    b = b_sav = 2.0;
    c = c_sav = 3.0f;
    
    auto all_byvalue = [=]() mutable
        { 
            a++; b++; c++;
        };    
    
    auto byvalue_xcept_a = [=, &a]() mutable
        { 
            a++; b++; c++;
        };    
    
    auto all_byreference = [&]() mutable
        { 
            a++; b++; c++;
        };    
    
    auto byreference_xcept_b = [&, b]() mutable
        { 
            a++; b++; c++;
        };    
    
    auto byreference_xcept_bc = [&, b, c]() mutable
        { 
            a++; b++; c++;
        };    

    std::cout << "all_byvalue()" << std::endl;
    all_byvalue();
    tell_change("a", a, a_sav); 
    tell_change("b", b, b_sav);
    tell_change("c", c, c_sav);
    std::cout << std::endl;
    restore(a, a_sav); restore(b, b_sav); restore(c, c_sav); 
    
    std::cout << "\nbyvalue_xcept_a()" << std::endl;
    byvalue_xcept_a();
    tell_change("a", a, a_sav); 
    tell_change("b", b, b_sav);
    tell_change("c", c, c_sav);
    std::cout << std::endl;
    restore(a, a_sav); restore(b, b_sav); restore(c, c_sav); 
    
    std::cout << "\nall_byreference()" << std::endl;
    all_byreference();
    tell_change("a", a, a_sav); 
    tell_change("b", b, b_sav);
    tell_change("c", c, c_sav);
    std::cout << std::endl;
    restore(a, a_sav); restore(b, b_sav); restore(c, c_sav); 
    
    std::cout << "\nbyreference_xcept_b()" << std::endl;
    byreference_xcept_b();
    tell_change("a", a, a_sav); 
    tell_change("b", b, b_sav);
    tell_change("c", c, c_sav);
    std::cout << std::endl;
    restore(a, a_sav); restore(b, b_sav); restore(c, c_sav); 

    std::cout << "\nbyreference_xcept_bc()" << std::endl;
    byreference_xcept_bc();
    tell_change("a", a, a_sav); 
    tell_change("b", b, b_sav);
    tell_change("c", c, c_sav);
    std::cout << std::endl;
    restore(a, a_sav); restore(b, b_sav); restore(c, c_sav); 
        
    return 0;
}
