#include <iostream>

// On the road to C++11 constants

// #define C_MATSIZE 10

const int cpp_matsize = 10;

constexpr int vec_size()
{
    return 10;
}

int main()
{
//    int no_matsize = 10;
//    char   buf[C_MATSIZE];
    double num[cpp_matsize];
//    float  fld[no_matsize];   
    int    ind[vec_size()];   
    
//    std::cout << "sizeof(buf) == " << sizeof(buf) << std::endl;
    std::cout << "sizeof(num) == " << sizeof(num) << std::endl;
//    std::cout << "sizeof(fld) == " << sizeof(fld) << std::endl;
    std::cout << "sizeof(ind) == " << sizeof(ind) << std::endl;
    
    return 0;
}

