#include <iostream>

class Base {
protected:
    int i;
public:
    Base() = delete;
    Base(int i) {
        std::cout << "Base::Base(int) called" << std::endl;
        this->i = i;
    }
    int get_i() {
        return i;
    }
};

class Deriv : public Base {
public:
//    Deriv() {
//        std::cout << "Deriv::Deriv() called" << std::endl;
//    }    
    Deriv( int i) : Base(i) {
        std::cout << "Deriv::Deriv(int) called" << std::endl;
    }
};

int main()
{
//    Base b;
    Deriv d(15);

    std::cout << "d.get_i() " << d.get_i() << std::endl;

    return 0;
}
