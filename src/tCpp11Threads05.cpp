// From https://www.cplusplus.com/reference/thread/thread/detach/

#include <iostream>       // std::cout, std::endl
#include <thread>         // class std::thread, 
                          //    std::this_thread::sleep_for()
#include <chrono>         // std::chrono::seconds()
 
void pause_thread(int n) 
{
  std::this_thread::sleep_for(std::chrono::seconds(n));
  std::cout << "pause of " << n << " seconds ended" << std::endl;
}
 
int main() 
{
  std::cout << "Will spawn and detach 3 threads..." << std::endl;
  
  std::thread (pause_thread, 1).detach();
  std::thread (pause_thread, 2).detach();
  std::thread (pause_thread, 3).detach();
  
  std::cout << "Done spawning 3 threads." << std::endl;

  std::cout << "\n(the main thread will now pause for 5 seconds)\n" 
            << std::endl;
  // give the detached threads time to finish (but not guaranteed!):
  pause_thread(5);
  
  std::cout << "\nProgram terminated" << std::endl;
  return 0;
}
