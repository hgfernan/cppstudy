#include <iostream>

// tLambda17 -- Tira-teima de captura por referência, todas

int main()
{
	int a = 1;
	float b = 2.0;
    auto func = [&]() { 
			if (a == 1) {
				std::cout << "a == 1" << std::endl;
				a++;
			}
			
			if (b == 2.0) {
				std::cout << "b == 2.0" << std::endl;
				b++;
			}
		};

    func();
        
    a++; b++;
    
    func();    
    
    std::cout << "2nd call: a == " << a << std::endl;
    std::cout << "2nd call: b == " << b << std::endl;

    return 0;
}

