#include <iostream>

// tLambda11-- Alteração de variáveis capturadas dentro da lambda
// ERRO

int main()
{
	int a = 1;
	float b = 2.0;
    auto func = [a, b]() { 
			if (a == 1) {
				std::cout << "a == 1" << std::endl;
				a++; 
			}
			
			if (b == 2.0) {
				std::cout << "b == 2.0" << std::endl;
				b++;
			}
		};

    func();
    
    std::cout << "a == " << a << std::endl;
    std::cout << "b == " << b << std::endl;

    return 0;
}

