#include <iostream>   // std::cout, std::endl

// Nested lambda expression definition
 
int main() 
{
	int    a = 1;
	double b = 2.0;
	float  c = 3.0f;
	
	auto inc_all = [&]() mutable
		{ 
			a++;
			
			[&b, &c]() {
				b++;
				
				[&c]() {
					c++;
				}();				
			}();
		};	
	
	std::cout << "Before inc_all(): " 
			  << "a " << a << ", "
			  << "b " << b << ", "
			  << "c " << c 
			  << std::endl;
	
	inc_all();
	
	std::cout << "After  inc_all(): " 
			  << "a " << a << ", "
			  << "b " << b << ", "
			  << "c " << c 
			  << std::endl;
	
	return 0;
}
