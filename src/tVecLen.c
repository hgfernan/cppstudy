#include <stdio.h>  /* printf() */
#include <stdlib.h> /* calloc(), free() */

#define vec_len(x) ( sizeof(x) / sizeof((x)[0]) )

int main(void)
{
	int ivec[3];
	int* pvec;
	double dvec[6];

	pvec = calloc(3, sizeof(int));
	
	printf("sizeof(ivec) %lu, sizeof(ivec[0]) %lu, vec_len(ivec) %lu\n", 
			sizeof(ivec), sizeof(ivec[0]), vec_len(ivec)
		  );
	printf("sizeof(pvec) %lu, sizeof(pvec[0]) %lu, vec_len(pvec) %lu\n", 
			sizeof(pvec), sizeof(pvec[0]), vec_len(pvec)
		  );
	printf("sizeof(dvec) %lu, sizeof(dvec[0]) %lu, vec_len(pvec) %lu\n", 
			sizeof(dvec), sizeof(dvec[0]), vec_len(dvec)
		  );
		  
	free(pvec);
	
	/* Normal function termination */
	return 0;
}
