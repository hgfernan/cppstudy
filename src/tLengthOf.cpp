// From https://en.cppreference.com/book/arrays

#include <cstddef>   // std:size_t
#include <iostream>  // std:cout, std::endl

template<typename T, std::size_t N>
std::size_t LengthOf(T (&)[N]) { return N; }

int main()
{
	int ivec[3];
//	int *pvec;
	double dvec[6];

//	pvec = new int[3];
	
	std::cout << "LengthOf(ivec) " << LengthOf(ivec) << std::endl;
//	std::cout << "LengthOf(pvec) " << LengthOf(pvec) << std::endl;
	std::cout << "LengthOf(dvec) " << LengthOf(dvec) << std::endl;
	
	// Normal function termination
	return 0;
}
