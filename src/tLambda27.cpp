#include <iostream>   // std::cout, std::endl

// Nested lambda expression invocation
 
int main() 
{
	int    a = 1;
	double b = 2.0;
	float  c = 3.0f;
	
	auto inc_a = [&a]() mutable
		{ 
			a++;
		};	
	
	auto inc_ab = [inc_a, &b]() mutable
		{ 
			inc_a();
			b++;
		};	
	
	auto inc_abc = [inc_ab, &c]() mutable
		{ 
			inc_ab();
			c++;
		};	
	
	std::cout << "Before inc_abc(): " 
			  << "a " << a << ", "
			  << "b " << b << ", "
			  << "c " << c 
			  << std::endl;
	
	inc_abc();
	
	std::cout << "After  inc_abc(): " 
			  << "a " << a << ", "
			  << "b " << b << ", "
			  << "c " << c 
			  << std::endl;
	
	return 0;
}
