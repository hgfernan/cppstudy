#include <iostream>   // std::cout, std::endl

// Changing the temporary copy of an unconstanted constant
 
int main() 
{
    const     int   a = 1;
	constexpr float b = 2.0f;
	
	auto show_const = [b]()
		{ 
            float& bref = const_cast<float&>(b); 
            bref++;
            
			std::cout << "in show_const() b " << bref << std::endl;
		};	
	
	show_const();
    std::cout << "in main() b " << b << std::endl;
	
	return 0;
}
