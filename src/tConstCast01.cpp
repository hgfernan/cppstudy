#include <iostream>

// auto inc(int& a) -> int
int inc(int& a)
{
    std::cout << "Inside inc() Before inc " << a << std::endl;
    a++;
    std::cout << "Inside inc() After  inc " << a << std::endl;
    
    return a;
}

int main()
{
    const     int a = 1;
    constexpr int b = a;
              int c = a;
              int rv;
    int v1[a];
    int v2[b];
    int v3[c];
    
    inc(a);
    
    std::cout << "Before inc() a " << a << std::endl;
    rv = inc(const_cast<int&>(a));
    std::cout << "inc() return " << rv << std::endl;
    std::cout << "After  inc() a " << a << std::endl;
    
    inc(b);
        
    std::cout << "\nBefore inc() b " << b << std::endl;
    rv = inc(const_cast<int&>(b));
    std::cout << "inc() return " << rv << std::endl;
    std::cout <<   "After  inc() b " << b << std::endl;

    std::cout << "\nA sole inc" << std::endl;
    inc(c);
        
    std::cout << "\nBefore inc() c " << c << std::endl;
    rv = inc(const_cast<int&>(c));
    std::cout << "inc() return " << rv << std::endl;
    std::cout <<   "After  inc() c " << c << std::endl;
    
    return 0;
}
