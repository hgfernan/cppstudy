#include <iostream>

class Base {
protected:
    int i;
public:
    Base() = delete;
    int get_i() {
        return i;
    }
};

class Deriv : public Base {
public:
    Deriv() {
        std::cout << "Deriv::Deriv() called" << std::endl;
    }    
};

int main()
{
//    Base b;
    Deriv d;

    std::cout << "d.get_i() " << d.get_i() << std::endl;

    return 0;
}
