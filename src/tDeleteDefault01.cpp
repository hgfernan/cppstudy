#include <iostream>

// First experiences with = delete and = default

class A {
protected:
    int i;
public:    
    A() = delete;
    A(const A&) = default;
};

class B : {
public:
    B() = default;
    B(const B&) = delete;
    B& operator= (const B&) = default; 
};

int main ()
{
    A a;
    std::cout << "a.i " << a.i << std::endl;
    a.i = 10;
    
    A a1(a);
    std::cout << "a1.i " << a1.i << std::endl;
    
    B b;
    b.i = 15;
    
//    B b1(b);
    
    B b2;
    b2 = b;
    std::cout << "b2.i " << b2.i << std::endl;
    
    return 0;
}
