set +x 

function compile() {
	name=$1
	echo ${name}
	g++ -std=c++11 -W -Wall -o ../bin/"${name}" "${name}".cpp \
		> ../bin/"${name}".err 2>&1 
}

for name in tLambda09 tLambda10 tLambda11 tLambda12 tLambda13 \
	tLambda14 tLambda15 tLambda16 tLambda17 tLambda18 tLambda19 \
	tLambda20 tLambda21 ; do
	compile ${name}
done

for file in ../bin/*.err ; do 
	size=$( du ${file} | awk '{ print $1}' )
	
	if [ ${size} -eq 0 ] ; then
		rm ${file}
	fi
done
