#include <iostream>

int main()
{
    auto func = [](){ std::cout << "Hello, world !" << std::endl; };

    func();

    return 0;
}

