#include <iostream>

// tLambda10-- Alteração de variáveis capturadas fora da lambda

int main()
{
	int a = 1;
	float b = 2.0;
    auto func = [a, b]() { 
			if (a == 1) {
				std::cout << "a == 1" << std::endl;
			}
			
			if (b == 2.0) {
				std::cout << "b == 2.0" << std::endl;
			}
		};

    func();
    
    a++; b++;
    
    func();    

    return 0;
}

