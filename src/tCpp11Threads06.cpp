//
// A thread sample to experiment with
// 
// From
// * https://cplusplus.com/reference/thread/thread/
// * https://cppsecrets.com/users/1442971101051149897110100971154850485464103109971051084699111109/C00-Benchmarking.php
//
#include <chrono>         // class high_resolution_clock, steady_clock
#include <thread>         // class std::thread, std::ref()
#include <iostream>       // std::cout, std::endl

class TinyBench
{
protected:
    std::chrono::time_point<std::chrono::high_resolution_clock> start_point;
    std::chrono::time_point<std::chrono::high_resolution_clock> end_point;

public:
    TinyBench()
    {
        // Normal function termination
        return;
    };

    std::chrono::time_point<std::chrono::high_resolution_clock> start()
    {
        start_point = std::chrono::high_resolution_clock::now(); // storing the starting time point in start
        auto result = start_point;

        // Normal function termination
        return result;
    };

    std::chrono::time_point<std::chrono::high_resolution_clock> stop()
    {
      end_point = std::chrono::high_resolution_clock::now(); // storing the starting time point in start
      auto result = end_point;

      // Normal function termination
      return result;
    };

    // TODO find out how to return duration<>

    double get_seconds_delta()
    {
      double result = std::chrono::duration_cast<
            std::chrono::duration<double> >(end_point - start_point).count();

      // Normal function termination
      return result;
    }

    long get_nanoseconds_delta()
    {
      double result = std::chrono::duration_cast<
            std::chrono::duration<long> >(end_point - start_point).count();

      // Normal function termination
      return result;
    }
};

double
pol(double x)
{
    double result;

    result = 10 + 10 * x + 10 * 10 * x * x;

    // Normal function termination
    return result;
}

double brute_minimum(double delta, int start, int final)
{
  double result = 5e+5;
  for (int num = start; num <= final; num++) {
    double x = num * delta;
    double rv = pol(x);
    if (rv < result) {
      result = rv;
    }
  }

  // Normal function termination
  return result;
}

void find_minimum(double delta, int start, int final, double& res)
{
  res = brute_minimum(delta, start, final);

  // Normal function termination
  return;
}

int main()
{
  TinyBench watch;

  int max_steps = 1000 * 1000 * 1000;

  double delta = 1.0 / max_steps;
  double res = 0;
  double res1 = 0;
  double res2 = 0;

  auto start = std::chrono::steady_clock::now();

  res = brute_minimum(delta, 0, max_steps);

  auto finish = std::chrono::steady_clock::now();
  double elapsed_seconds =
      std::chrono::duration_cast<std::chrono::duration<double>>(finish - start).count();

  std::cout << "Main thread found the minimum for the polynomial: " << res << std::endl;
  std::cout << "Elapsed time " << elapsed_seconds << std::endl;

  start = std::chrono::steady_clock::now();

  // spawn new threads that call brute_minimum()
  std::thread first(find_minimum, delta, 0            , max_steps / 2, std::ref(res1));
  std::thread secnd(find_minimum, delta, max_steps / 2, max_steps    , std::ref(res2));

  std::cout << "main, foo and bar now execute concurrently...\n";

  // synchronize threads:
  first.join();               // pauses until first finishes
  secnd.join();               // pauses until second finishes

  finish = std::chrono::steady_clock::now();
  elapsed_seconds =
      std::chrono::duration_cast<std::chrono::duration<double>>(finish - start).count();

  std::cout << "foo and bar completed.\n";

  std::cout << "Thread 1 found the minimum for polynomial " << res1 << std::endl;
  std::cout << "Thread 2 found the minimum for polynomial " << res2 << std::endl;
  std::cout << "Elapsed time " << elapsed_seconds << std::endl;

  return 0;
}
