#include <iostream>

// tLambda16 -- Captura por referência, todas -- para sintaxe

int main()
{
	int a = 1;
	float b = 2.0;
    auto func = [&]() { 
			if (a == 1) {
				std::cout << "a == 1" << std::endl;
			}
			
			if (b == 2.0) {
				std::cout << "b == 2.0" << std::endl;
			}
		};

    func();    
    
    std::cout << "outside: a == " << a << std::endl;
    std::cout << "outside: b == " << b << std::endl;

    return 0;
}

