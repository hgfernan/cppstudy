#include <iostream>   // std::cout, std::endl

// Consts need to be in the capture list, if a change is attempted
 
int main() 
{
              int   a = 1;
	constexpr float b = 2.0f;
	
    auto show_const = [&a, &b]() mutable
//	auto show_const = [a, b]() mutable
//	auto show_const = [a, b]()
//	auto show_const = [a]() 
		{ 
            a++; 
            b++;
            
			std::cout << "show_const() a " << a << std::endl;
			std::cout << "show_const() b " << b << std::endl;
		};	
	
	show_const();
    	
	return 0;
}
