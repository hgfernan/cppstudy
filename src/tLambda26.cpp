#include <iostream>   // std::cout, std::endl
#include <string>     // class std::string
#include <functional> // template std::function<>

// Creation of a test functio to invoke all cases

template <typename T>
	void
	tell_change(const char* title, T current, T previous) {
		std::cout << title << " ";
		if (current == previous) {
			std::cout << "didn't change";
		} else {
			std::cout << "changed from " 
					  << previous << " to " << current;
		}
		
		std::cout << ", ";
	}

template <typename T> 
	void
	restore(T& current, T& previous) {
		current = previous;
	}
	
struct val_sav {
	int    *pa, *pa_sav;
	double *pb, *pb_sav;
	float  *pc, *pc_sav;
};

void test_lambda(const std::string& name, 
				 std::function<void(void)> lambda, 
				 val_sav& vs) 
{
	std::cout << "\n" << name<< std::endl;
	lambda();
	
	tell_change("a", *vs.pa, *vs.pa_sav); 
	tell_change("b", *vs.pb, *vs.pb_sav);
	tell_change("c", *vs.pc, *vs.pc_sav);
	std::cout << std::endl;
	
	restore(*vs.pa, *vs.pa_sav); 
	restore(*vs.pb, *vs.pb_sav); 
	restore(*vs.pc, *vs.pc_sav); 
}
 
int main() 
{
	int    a, a_sav;
	double b, b_sav;
	float  c, c_sav;
	
	a = a_sav = 1;
	b = b_sav = 2.0;
	c = c_sav = 3.0f;
	
	auto all_byvalue = [=]() mutable
		{ 
			a++; b++; c++;
		};	
	
	auto byvalue_xcept_a = [=, &a]() mutable
		{ 
			a++; b++; c++;
		};	
	
	auto all_byreference = [&]() mutable
		{ 
			a++; b++; c++;
		};	
	
	auto byreference_xcept_b = [&, b]() mutable
		{ 
			a++; b++; c++;
		};	
	
	auto byreference_xcept_bc = [&, b, c]() mutable
		{ 
			a++; b++; c++;
		};	

	val_sav vs;
	vs.pa     = &a;
	vs.pa_sav = &a_sav;
	vs.pb     = &b;
	vs.pb_sav = &b_sav;
	vs.pc     = &c;
	vs.pc_sav = &c_sav;
	
	test_lambda("all_byvalue()", all_byvalue, vs);
	
	test_lambda("byvalue_xcept_a()", byvalue_xcept_a, vs);
	
	test_lambda("all_byreference()", all_byreference, vs);

	test_lambda("byreference_xcept_b()", byreference_xcept_b, vs);

	test_lambda("byreference_xcept_bc()", byreference_xcept_bc, vs);

	return 0;
}
