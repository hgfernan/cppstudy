#include <iostream>
#include <functional>
#include <vector>

int how_many(std::vector<int> v, std::function<bool(int)> f)
{
    int result = 0;

    for (int value : v) {
        result += f(value) ? 1 : 0;
    }

    return result;
}

int main() {
    std::vector<int> v1 { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
    std::vector<int> v2 = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
    
    int total_even = how_many(v1,
                        [](int v) { return (v % 2) == 0;});

    std::cout << "Count of even values: " << total_even << std::endl;
    
    int total_odd = how_many(v2,
                        [](int v) { return (v % 2) == 1;});

    std::cout << "Count of odd values: " << total_odd << std::endl;

    return 0;
}
