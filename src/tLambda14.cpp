#include <iostream>

// tLambda14 -- Captura por referência, mutable

int main()
{
	int a = 1;
	float b = 2.0;
    auto func = [&a, &b]() mutable { 
			if (a == 1) {
				std::cout << "a == 1" << std::endl;
				a++; 
				std::cout << "inside: a == " << a << std::endl;
			}
			
			if (b == 2.0) {
				std::cout << "b == 2.0" << std::endl;
				b++;
				std::cout << "inside: b == " << b << std::endl;
			}
		};

    func();
    
    std::cout << "outside: a == " << a << std::endl;
    std::cout << "outside: b == " << b << std::endl;

    return 0;
}

