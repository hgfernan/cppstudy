#include <iostream>

// tLambda12-- Alteração de variáveis capturadas na lambda, mutable

int main()
{
	int a = 1;
	float b = 2.0;
    auto func = [a, b]() mutable { 
			if (a == 1) {
				std::cout << "a == 1" << std::endl;
				a++; 
			}
			
			if (b == 2.0) {
				std::cout << "b == 2.0" << std::endl;
				b++;
			}
		};

    func();
    
    std::cout << "outside: a == " << a << std::endl;
    std::cout << "outside: b == " << b << std::endl;

    return 0;
}

