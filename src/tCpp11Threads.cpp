// From https://twitter.com/xchatty/status/1281720773703540737/photo/1

#include <iostream>
#include <atomic>
#include <thread>
#include <vector>

std::atomic<int> v(0);
int x(0);

void adder()
{
    for (int i = 0; i < 1000 * 1000 ; i++){
        v += 1;
        x += 1;
    }
}

int main()
{
	unsigned max_threads = std::thread::hardware_concurrency();
	
	std::vector<std::thread *> t(max_threads);
    
    for(unsigned i = 0; i < max_threads; i++){
        std::cout << "Started thread i = " << i << std::endl;
        t[i] = new std::thread(adder);
    }
    
    for(unsigned i = 0; i < max_threads; i++){
        t[i]->join();
    }
    
    std::cout << "v = " << v << std::endl;
    std::cout << "x = " << x << std::endl;
    std::cout << "x / v ratio: " << (1.0 * x / v ) << std::endl;
    
    return 0;
}
