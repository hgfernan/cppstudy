#include <iostream>

//~ constexpr 
size_t arr_size() 
{
	return 10;
}

int main() 
{
	int sz = 10;
	
	//~ auto constexpr func = [sz]() { return sz; };
	
	int arr1[arr_size()]  = {1};
	//~ int arr2[func()] = {2};
	
	std::cout << "arr1[0] " << arr1[0] << std::endl;
	std::cout << "arr1[1] " << arr1[1] << std::endl;
	
	//~ std::cout << "arr2[0] " << arr2[0] << std::endl;
	//~ std::cout << "arr2[1] " << arr2[1] << std::endl;
	
	return 0;
}
