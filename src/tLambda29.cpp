#include <iostream>   // std::cout, std::endl

// Const need not to be in the capture list
 
int main() 
{
	const     int   a = 1;
	constexpr float b = 2.0f;
	
	auto show_const = []()
		{ 
			std::cout << "a " << a << std::endl;
			std::cout << "b " << b << std::endl;
		};	
	
	show_const();
	
	return 0;
}

