#include <iostream>   // std::cout, std::endl

// Consts unconstanted need to be in the capture list
 
int main() 
{
              int   a = 1;
	constexpr float b = 2.0f;
	
    auto show_const = [a, b]() mutable
		{ 
            a++;
            const_cast<float&>(b)++;
            
			std::cout << "show_const() a " << a << std::endl;
			std::cout << "show_const() b " << b << std::endl;
		};	
	
	show_const();

    std::cout << "main() a " << a << std::endl;
    std::cout << "main() b " << b << std::endl;
    	
	return 0;
}
