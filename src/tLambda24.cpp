#include <iostream>

// Summary of lambda expression experiences

int main() 
{
	int varPhoto           = 10;
	int varCallByValue     = 10;
	int varCallByReference = 10;

	auto funcPhoto = [varPhoto]() 
		{ 
			std::cout << "Photo:           var " << varPhoto << std::endl; 
			//~ varPhoto++;
		};

	auto funcCallByValue = [varCallByValue]() mutable 
		{ 
			std::cout << "CallByValue:     var " << varCallByValue << std::endl; 
			varCallByValue++; 
		};

	auto funcCallByReference = [&varCallByReference]() mutable 
		{ 
			std::cout << "CallByReference: var " << varCallByReference << std::endl; 
			varCallByReference++; 
		};
	
	std::cout << "First round" << std::endl;
	
	funcPhoto();
	funcCallByValue();
	funcCallByReference();
	
	std::cout << "\nIncrementing var" << std::endl;
	varPhoto++;
	varCallByValue++;
	varCallByReference++;
	
	std::cout << "\nSecond round" << std::endl;
	
	funcPhoto();
	funcCallByValue();
	funcCallByReference();
	
	return 0;
}
