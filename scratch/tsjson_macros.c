/**
 * Test of simple macros to present JSON data using a string buffer
 * Created on Thu Feb  6 17:14:29 2025
 * 
 * @author: hilton
 * 
 */

#include "sjson_macros.h" 

int main(void) {
	char buf[1024] = {'\0'};
	SJSON_MACROS_VARS
	
	SJM_OPEN_ALL(buf);
	
		SJM_OPEN_STRUCT("parameters", buf);
		
			SJM_OPEN_STRUCT("reading", buf);
				SJM_ADD_PAIR_STR("inp", "", buf);
			SJM_CLOSE_STRUCT(buf);
		
			SJM_OPEN_STRUCT("init", buf);
				SJM_ADD_PAIR_INT("L", 5, buf);
				SJM_ADD_PAIR_INT("n_rows", 10, buf);
				SJM_ADD_PAIR_INT("method", 5, buf);
			SJM_CLOSE_STRUCT(buf);

			SJM_OPEN_STRUCT("fitting", buf);
				SJM_ADD_PAIR_STR("obj", "", buf);
				SJM_ADD_PAIR_STR("inp", "", buf);
			SJM_CLOSE_STRUCT(buf);
		
			SJM_OPEN_STRUCT("summary", buf);
				SJM_ADD_PAIR_STR("obj", "", buf);
			SJM_CLOSE_STRUCT(buf);

			SJM_OPEN_STRUCT("predict", buf);
				SJM_ADD_PAIR_STR("obj", "", buf);
				SJM_ADD_PAIR_STR("inp", "", buf);
				SJM_ADD_PAIR_STR("L", "", buf);
				SJM_ADD_PAIR_STR("xpred", "", buf);
				SJM_ADD_PAIR_STR("amse", "", buf);
			SJM_CLOSE_STRUCT(buf);
		
		SJM_CLOSE_STRUCT(buf);
	
		SJM_OPEN_STRUCT("result", buf);

			SJM_OPEN_STRUCT("fitting", buf);
				SJM_ADD_PAIR_INT("aic", 7, buf);
				SJM_ADD_PAIR_INT("order", 7, buf);
				
				SJM_OPEN_ARRAY("coefficients", buf);
					SJM_ADD_ELMT_DBL(0.372554, buf);
					SJM_ADD_ELMT_DBL(0.197209, buf);
					SJM_ADD_ELMT_DBL(0.0202597, buf);
					SJM_ADD_ELMT_DBL(0.013501, buf);
					SJM_ADD_ELMT_DBL(-0.0148193, buf);
					SJM_ADD_ELMT_DBL(0.0625254, buf);
					SJM_ADD_ELMT_DBL(0.156202, buf);
				SJM_CLOSE_ARRAY(buf);

				SJM_ADD_PAIR_DBL("mean", 17.0624, buf);
				SJM_ADD_PAIR_DBL("sigma2", 0.0949526, buf);
				
			SJM_CLOSE_STRUCT(buf);
			
			SJM_OPEN_STRUCT("predict", buf);
			
				SJM_OPEN_ARRAY("xpred", buf);
					SJM_ADD_ELMT_DBL(17.4489, buf);
					SJM_ADD_ELMT_DBL(17.4853, buf);
					SJM_ADD_ELMT_DBL(17.4255, buf);
					SJM_ADD_ELMT_DBL(17.4464, buf);
					SJM_ADD_ELMT_DBL(17.3939, buf);
				SJM_CLOSE_ARRAY(buf);
			
				SJM_OPEN_ARRAY("amse", buf);
					SJM_ADD_ELMT_DBL(0.304906, buf);
					SJM_ADD_ELMT_DBL(0.325378, buf);
					SJM_ADD_ELMT_DBL(0.341126, buf);
					SJM_ADD_ELMT_DBL(0.347595, buf);
					SJM_ADD_ELMT_DBL(0.351388, buf);
				SJM_CLOSE_ARRAY(buf);
			
			SJM_CLOSE_STRUCT(buf);
		
		SJM_CLOSE_STRUCT(buf);
	
	SJM_CLOSE_ALL(buf);
	
	puts(buf);

	/* Normal function termination */
	return 0;
}
