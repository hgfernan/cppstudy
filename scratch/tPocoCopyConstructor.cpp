#include <iostream>

struct Poco {
	int iVal;
	double dVal;	
};

int main() 
{
	Poco poco1{10, 0.10};

	std::cout << "poco1: iVal " << poco1.iVal << ", dVal " << poco1.dVal << std::endl;

	std::cout << "\nWill copy construct poco2 from poco1" << std::endl;
	Poco poco2(poco1);

	std::cout << "poco2: iVal " << poco2.iVal << ", dVal " << poco2.dVal << std::endl;

	return 0;
} 
