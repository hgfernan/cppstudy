/**
 * Some insertion operators
 * Created on Fri Apr 26 22:03:11 2024
 *
 * @author: user
 *
 **/
#ifndef INSERTION_OP_HPP_INCLUDED
#define INSERTION_OP_HPP_INCLUDED

#include<iostream>
#include<sstream>  /// class std::stringstream
#include<map>
#include<list>
#include<vector>
#include<unordered_map>

/// \todo Add map, list and other containers, including common array

/// \todo Check if there's an interface with to_string()

/// \todo Add a generic insertion op using instance begin() and end()

template<typename T>
std::ostream& operator<<(std::ostream& s, const std::list<T>& l)
{
    std::string buff("[");
    std::stringstream scratchpad;

    if (l.size() > 0) {
		for (auto it = l.cbegin(); it != l.cend(); it++) {
			scratchpad << *it << ", ";
		}

		std::string temp = scratchpad.str();
		buff += temp.substr(0, temp.length() - 2);
	}

	buff += "]";
    return s << buff;
}

template<typename K, typename V>
std::ostream& operator<<(std::ostream& s, const std::map<K, V>& m)
{
    s.put('{');

    std::size_t nelems = m.size();
    if (nelems > 0) {
        auto it = m.cbegin();
		for (std::size_t elemno = 0; elemno < (nelems - 1); elemno++) {
			s << it->first << ": " << it->second << ", ";
            it++;
		}

        s << it->first << ": " << it->second;
	}
    return s << "}";
}

template<typename T>
std::ostream& operator<<(std::ostream& s, const std::vector<T>& v)
{
    s.put('[');

    if (v.size() > 0) {
		for (std::size_t ind = 0; ind < v.size() - 1; ind++) {
			s << v[ind] << ", ";
		}

		s << v[v.size() - 1];
	}
    return s << "]";
}

template<typename K, typename V>
std::ostream& operator<<(std::ostream& s, const std::unordered_map<K, V>& m)
{
    s.put('{');

    std::size_t nelems = m.size();
    if (nelems > 0) {
        auto it = m.cbegin();
		for (std::size_t elemno = 0; elemno < (nelems - 1); elemno++) {
			s << it->first << ": " << it->second << ", ";
            it++;
		}

        s << it->first << ": " << it->second;
	}
    return s << "}";
}

#endif // INSERTION_OP_HPP_INCLUDED
