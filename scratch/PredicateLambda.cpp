/**
 * Sample for predicate lambda in std::replace()
 * Created on Mon Jun 17 10:20:30 2024
 * 
 * @author: Makulik Günther 
 * From: 
 * * https://gist.github.com/makulik/9425382    
 *
 */

#include <iostream>
#include <string>
#include <algorithm>

int main() {

	std::string s = "xAAAyyByABCxGxZyIxGyyZI";

	std::cout << "Before: '" << s << "'" << std::endl;

	bool (*pred1)(char) = [](char c) { return c == 'x' || c == 'y'; };
	std::replace_if(s.begin(),s.end(),pred1,'*');
	std::cout << "1st tr: '" << s << "'" << std::endl;

	auto pred2 = [](char c) { return c == '*'; };
	std::replace_if(s.begin(),s.end(),pred2,'#');
	std::cout << "2nd tr: '" << s << "'" << std::endl;

	return 0;
}

