/**
 * Trying to compare difference against sizeof
 * Created on Mon Jan 13 11:33:07 2025
 * 
 * @author: hilton
 *
 **/
 
#include <stdio.h>

int x_g[10] = {0};
char c_g = '\0';

int main()
{
	char c_l2 = '\0';
	int x_l[10] = {0};
	char c_l = '\0';
	
	printf("sizeof(x_g) %lu, diff &c_g - x_g %Ld\n", 
		sizeof(x_g), (long long int)(&c_g - (char*)x_g));
	
	printf("sizeof(x_l) %lu, diff x_l - &c_l %Ld\n", 
		sizeof(x_l), (long long int)((char*)x_l - &c_l));
	
	printf("diff &c_l2 - &c_l %Ld\n", 
		(long long int)(&c_l2 - &c_l));
	
	printf("diff &c_l - &c_l2 %Ld\n", 
		(long long int)(&c_l - &c_l2));
		
	return 0;
}
