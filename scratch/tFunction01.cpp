/**
 * Sample code to test std::function<>
 * Created on Tue Apr 30 21:44:53 2024
 *
 * @author: @wohlstad
 *
 * From:
 * -- https://stackoverflow.com/questions/74413185/how-to-correctly-pass-a-function-with-parameters-to-another-function
 *
 */

#include <iostream>
#include <functional>

int thirds(int a)
{
    return a + 1;
}

template <typename T, typename B>
//------------------------------------------------VVVVV-
int hello(T x, B y, std::function<int(int)> func, int a)
{
    int first = x + 1;
    int second = y + 1;
    int third = func(a);
    return first + second + third;
}

int add()
{
    std::function<int(int)> myfunc = thirds;
    return hello(1, 1, myfunc, 6);
}

int main()
{
    std::cout << add() << std::endl;
    return 0;
}
