#include <stdio.h> 
#include <math.h>

typedef double (*cmp) (double, double);

int main() 
{
	cmp func;
	double left = 10, right = 15;

	puts("fmin");
	func = fmin;
	printf("One extrema of %f and %f is %f\n", left, right, func(left, right));

	puts("fmax");
	func = fmax;
	printf("One extrema of %f and %f is %f\n", left, right, func(left, right));
	
	return 0;
}

