#include <iostream>

#define ON_INPUT "input"
#define OS_INPUT "i"

#define OJ_INPUT ON_INPUT "," OS_INPUT

int main()
{
    std::cout << "Option  long   '" << ON_INPUT << "'" << std::endl;
    std::cout << "Option  short  '" << OS_INPUT << "'" << std::endl;
    std::cout << "Options joined '" << OJ_INPUT << "'" << std::endl;
    
    return 0;
}
