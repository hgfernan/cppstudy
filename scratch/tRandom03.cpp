/**
 * Generation of a sample with fixed seed
 * Created on Sat Apr 13 18:28:46 2024
 * 
 * @author: user
 * From:
 * - https://en.cppreference.com/w/cpp/numeric/random
*/

#include <random>
#include <vector>
#include <iomanip>
#include <iostream>
 
template<typename T>
std::ostream& operator<<(std::ostream& s, const std::vector<T>& v)
{
    s.put('{');
    
    if (v.size() > 0) {
		for (std::size_t ind = 0; ind < v.size() - 1; ind++) {
			s << v[ind] << ", ";
		}
		
		s << v[v.size() - 1];
	}
    return s << "}";
}

int main()
{
    std::seed_seq seed2{3841299750ul,  647485403ul, 1492458493ul, 
						1211501403ul, 2007303644ul, 3678919580ul, 
						3346392214ul,   68269330ul};
    std::mt19937 gen(seed2);
    std::uniform_int_distribution<> distrib(1, 6);
    
    for (int n = 0; n != 10; ++n) {
        std::cout << distrib(gen) << ' ';
    }
    std::cout << std::endl;
    
	return 0;
}
