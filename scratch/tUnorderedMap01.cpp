/**
 * Failed attempt of transforming insert into a sequential number generator
 * Created on Thu Apr 25 15:36:19 2024
 *
 * @author: user
 *
 **/

#include <random> // random_device, default_random_device...
#include <iostream>
#include <unordered_map>

int main()
{
    std::size_t buf_siz = 11;
    char alphabet[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                      "abcdefghijklmnopqrstuvwxyz"
                      "0123456789"
                      "_";
    // Seed with a real random value, if available
    std::random_device r;

    std::cout << "r() " << r() << std::endl;

    // Choose a random mean between 1 and 6
    std::default_random_engine e1(r());
    std::uniform_int_distribution<int> uniform_dist(0, sizeof(alphabet) - 1);

    std::unordered_map<std::string, std::size_t> smap;

    for (std::size_t count = 0; count < 10; count++) {
        char* buf = new char[buf_siz];
        for (std::size_t nchar = 0; nchar < buf_siz - 1; nchar++) {
            std::size_t ind = uniform_dist(e1);
            std::cout << "\t\t nchar " << nchar
                      << ", ind " << ind << std::endl;
            buf[nchar] = alphabet[ind];
        }
        buf[buf_siz - 1] = '\0';
        std::string* key = new std::string(buf);

        std::cout << "key '" << *key << "', count " << count << std::endl;

        auto rv = smap.insert(std::make_pair(*key, count));
        std::cout << std::boolalpha
                  << "\tInserted: " << rv.second
//                  << ", iterator " << static_cast<std::size_t>(rv.first)
                  << std::endl;

        auto diff = std::distance(rv.first, smap.begin());
        std::cout << "\tposition " << diff << std::endl;
    }


    /// Normal function termination
    return 0;
}
