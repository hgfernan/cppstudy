/**
* Adapting Karunesh Johri's code to recognize a C decimal number
* Created on Sun Dec 29 01:25:50 2024
* 
* @author: hilton
* From:
* -- https://www.softprayog.in/programming/regular-expressions-in-c
*
**/

#include <stdio.h>
// #include <stdint.h>  /* intmax_t */
#include <stdlib.h> /* EXIT_FAILURE, exit() */
#include <string.h> /* strcpy() */
#include <regex.h>  /* */

#define ARRAY_SIZE(x)  ( sizeof(x) / sizeof(x[0]) )

#define NUMERIC_MASK "[+-]?[0-9]\\.?[0-9]*(e[+-]?[0-9]+)?"

char* 
deblank(const char* input) 
{
	int start = 0;
	int stop = 0;
	size_t length = 0;
	size_t full_length = 0;
	char* result = (char*)NULL;
	
	if (!input) return NULL;
	
	full_length = strlen(input);
	if (0 == full_length) {
		return (char*)calloc(1, sizeof(char*));
	}
	
	for (start = 0; *(input + start) == ' '; start++)
		;
	
	for (stop = full_length - 1; stop > 0; stop--) {
		if (*(input + stop) != ' ') break;
	} 
	
	length = full_length - start + 1; 
	length -= full_length - stop;
	
	result = (char*)calloc(length + 1, sizeof(char));
	strncpy(result, input + start, length);
	
	/* Normal function termination */
	return result;
}

int 
is_numeric(const char* input) 
{
	int rv;
	int result;
	char* deb = (char*)NULL;
	regex_t regex;
	regmatch_t pmatch[3];         // Up to 3 sub-expressions
	
	rv = regcomp (&regex, NUMERIC_MASK, REG_EXTENDED | REG_ICASE | REG_NEWLINE);
	if (rv) {
		return 0;
	}

	/* printf("input '%s'\n", input); */
	deb = deblank(input);
	
	/* printf("deb '%s'\n", deb); */
	rv = regexec (&regex, deb, ARRAY_SIZE (pmatch), pmatch, 0);
	
	if (rv) {
		return 0;
	}
	
	result = (int)
		(strlen(deb) == (size_t)(pmatch[0].rm_eo - pmatch[0].rm_so));
	
	/* Normal function termination */
	return result; 
}

int main(int argc, char* const* argv) 
{
	int rv;
	char re[128] = "\0";
	char buf[1024]; 
	char* text = (char*)NULL; 
	size_t ind = 0;
	regex_t regex;
	regmatch_t pmatch[3];         // Up to 3 sub-expressions
	
	if (argc < 2) {
		printf("%s: ERROR Text to test was not informed\n",
			argv[0]);
		return EXIT_FAILURE;
	}
	
	printf("ARRAY_SIZE(pmatch) %lu\n", ARRAY_SIZE(pmatch));
	
	text = argv[1];
	strcpy(re, NUMERIC_MASK);
	
	rv = regcomp (&regex, NUMERIC_MASK, REG_EXTENDED | REG_ICASE | REG_NEWLINE);
	
	if (rv) {
		(void)regerror (rv, &regex, buf, sizeof (buf));
		fprintf (stderr, "Error: regcomp: %s\n", buf);
		exit (EXIT_FAILURE);
	}
	
	rv = regexec (&regex, text, ARRAY_SIZE (pmatch), pmatch, 0);
	if (rv) {
		if (rv != REG_NOMATCH) {
			(void) regerror (rv, &regex, buf, sizeof (buf));
			fprintf (stderr, "Error: regexec: %s\n", buf);
			
			exit (EXIT_FAILURE);
		}
		printf("Expression '%s' didn't match regular expression '%s'\n",
			text, re);
	} else {
		printf("Expression '%s' matched regular expression '%s'\n", 
			text, re);
		
		for (ind = 0; ind < ARRAY_SIZE(pmatch); ind++) {
			int length = pmatch[ind].rm_eo - pmatch[ind].rm_so;
			if (-1 == pmatch[ind].rm_so) break;
			
			printf("\tso %d, eo %d subs '%.*s'\n", 
					pmatch[ind].rm_so, pmatch[ind].rm_eo,
					length, text + pmatch[ind].rm_so);
					
		}
	}
	
	printf("\n-- Is '%s' numeric ?\n", text);
	rv = is_numeric(text);
	printf("-- %s\n", (rv? "True" : "False"));

	/* Normal function termination */
	return 0;
}
