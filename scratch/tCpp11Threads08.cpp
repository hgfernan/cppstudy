/*
 * Created on Sat Jan 21 17:55:35 2023
 * @author: Alex B form StackOverflow.com
 * From: 
 * * https://stackoverflow.com/questions/7686939/c-simple-return-value-from-stdthread 
 */

#include <iostream> // std::cout, std::endl
#include <thread>   // std::async()
#include <future>   // std::promise<T>

// void func(std::promise<int> && p) {
void func(std::promise<int> & p) {
    p.set_value(1);
}

int main()
{
    std::promise<int> p; 
    auto f = p.get_future();
    // std::thread t(func, std::move(p));
    std::thread t(func, std::ref(p));
    t.join();
    int i = f.get();

    std::cout << "Promise return value " << i << std::endl;

    return 0;
}
