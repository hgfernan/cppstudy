/**
 * Use of extraction operator to read a file till EOF
 * Created on Tue Apr 16 03:52:44 2024
 * 
 * @author: Patrick Loz
 * From: 
 * - https://stackoverflow.com/questions/21647/reading-from-text-file-until-eof-repeats-last-line
 *
 **/

#include <string> 
#include <fstream> 
#include <iostream> 

int main() 
{
    std::ifstream iFile("input.txt");
    std::string line;

    while (iFile >> line) 
    {
        std::cout << line << std::endl;
    }

    // Normal function termination
    return 0;
}
