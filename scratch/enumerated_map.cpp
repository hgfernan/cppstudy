/**
 * Implementation of class EnumeratedMap
 * Created on Fri Apr 26 22:07:08 2024
 *
 * @author: user
 *
 **/

#include <iostream>
#include "enumerated_map.hpp"

std::size_t EnumeratedMap::size() const
{
    if (m_keyToSizeT.size() != m_itemStore.size()) {
        std::cerr << "**** INTERNAL ERROR Stores with different sizes ! ***"
                  << std::endl;
    }

    return m_keyToSizeT.size();
}

/** \brief Add a pair of key and item to the enumerated map
 *
 * \param key to the map
 * \param item of data to add to vector store
 * \return pair of boolean to indicate success, and ordinal if successful
 *
 */
std::pair<bool, size_t>
EnumeratedMap::addPair (std::string key, std::size_t recno)
{
    std::size_t uiIntKey;
    auto rFind = m_keyToSizeT.find(key);

    /// If key was not found
    if (m_keyToSizeT.end() == rFind) {
        uiIntKey = m_keyToSizeT.size();

        auto rInsert = m_keyToSizeT.insert({key, uiIntKey});
        if (! rInsert.second) {
            return std::make_pair(false, static_cast<std::size_t>(-1));
        }

        std::vector<std::size_t> item{recno};

        auto rInsert2 = m_itemStore.insert({uiIntKey, item});
        if (! rInsert2.second) {
            return std::make_pair(false, static_cast<std::size_t>(-1));
        }

        return std::make_pair(true, uiIntKey);
    }

    /// Key was found, ordinal will be added to vector store
    uiIntKey = rFind->second;

    auto rFind2 = m_itemStore.find(uiIntKey);
    if (m_itemStore.end() == rFind2) {
        return std::make_pair(false, static_cast<std::size_t>(-1));
    }

    rFind2->second.push_back(recno);

    /// Normal function termination
    return std::make_pair(true, uiIntKey);
}

