/**
 * Declaration and validation of an option type in the context of boost's program_option
 * Created on Mon Apr 22 20:23:33 2024
 *
 * @author: @Francois, revised by @Lars
 * From:
 * - https://stackoverflow.com/questions/3324285/boostprogram-options-how-to-declare-and-validate-my-own-option-type-when-it
 **/

#include <iostream>

#include <boost/program_options.hpp>
using namespace boost;
using namespace boost::program_options;

struct my_type {
    my_type(int nn) : n(nn) {}
    int n;
};

void validate(boost::any& v,
              const std::vector<std::string>& values,
              my_type*, int)  {
    const std::string& s = validators::get_single_string(values);
    v = any(my_type(lexical_cast<int>(s)));
}

int main(int argc, char* const* argv)
{
    options_description desc("options");
    desc.add_options()
        ("type", value<my_type>()    , "")
    ;

    variables_map vm;
    store(
        parse_command_line(argc, argv, desc),
        vm);
    notify(vm);

    if (argc == 1 || vm.count("help")) {
        std::cout << desc << std::endl;
        std::exit(EXIT_SUCCESS);
    }

    if (vm.count("type")) {
        std::cout << "type was set to "
             << vm["type"].as<int>() << ".\n";
    } else {
        std::cout << "type was not set.\n";
    }

    return 0;
}
