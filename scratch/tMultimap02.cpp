/**
 * Use of multimap::insert() and multimap::equal_range()
 * Created on Fri Apr 19 18:32:21 2024
 *
 * @author: cppreference.com
 * From:
 * -- https://en.cppreference.com/w/cpp/container/multimap/insert
 * -- https://en.cppreference.com/w/cpp/container/multimap/equal_range
 **/

#include <functional> // std::greater<int>
#include <iostream>
#include <map> // class std::multimap
#include <string>
#include <string_view>
#include <utility> // list initialization

template<class M>
void print(const std::string_view rem, const M& mmap)
{
    std::cout << rem << ' ';
    for (const auto& e : mmap)
        std::cout << '{' << e.first << ',' << e.second << "} ";
    std::cout << '\n';
}

int main()
{
    // list-initialize
    std::cout << "List-initialize" << std::endl;
    std::multimap<int, std::string, std::greater<int>> mmap
        {{2, "foo"}, {2, "bar"}, {3, "baz"}, {1, "abc"}, {5, "def"}};
    print("#1", mmap);

    // insert using value_type
    std::cout << "\ninsert using value_type" << std::endl;
    mmap.insert(decltype(mmap)::value_type(5, "pqr"));
    print("#2", mmap);

    // insert using pair
    std::cout << "\ninsert using pair explicitly" << std::endl;
    mmap.insert(std::pair{6, "uvw"});
    print("#3", mmap);

    std::cout << "\ninsert using pair implicitly" << std::endl;
    mmap.insert({7, "xyz"});
    print("#4", mmap);

    // insert using initializer_list
    std::cout << "\ninsert using initializer_list" << std::endl;
    mmap.insert({{5, "one"}, {5, "two"}});
    print("#5", mmap);

    // insert using a pair of iterators
    // mmap.clear();
    std::cout << "\ninsert using a pair of iterators" << std::endl;
    const auto il = {std::pair{1, "ä"}, {2, "ё"}, {2, "ö"}, {3, "ü"}};
    mmap.insert(il.begin(), il.end());
    print("#6", mmap);

    std::cout << "\nerase using an equal range, individual iterators" << std::endl;
    auto range = mmap.equal_range(5);

    for (auto i = range.first; i != range.second; ) {
        std::cout << "Will delete " << i->first
                  << ": " << i->second << std::endl;
        i = mmap.erase(i);
    }
    print("#7", mmap);

    std::cout << "\nerase using an equal range, a pair of iterators" << std::endl;
    range = mmap.equal_range(2);

    mmap.erase(range.first, range.second);

    std::cout << "\nfinal multimap" << std::endl;
    print("#8", mmap);

    // Normal function termination
    return 0;
}

