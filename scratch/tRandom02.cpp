/**
 * Just to print a randomize seed
 * Created on Sat Apr 13 17:52:12 2024
 * 
 * @author: @cppreference
 * From 
 * - https://en.cppreference.com/w/cpp/numeric/random
**/

#include <random>
#include <vector>
#include <iomanip>
#include <iostream>
 
template<typename T>
std::ostream& operator<<(std::ostream& s, const std::vector<T>& v)
{
    s.put('{');
    
    if (v.size() > 0) {
		for (std::size_t ind = 0; ind < v.size() - 1; ind++) {
			s << v[ind] << ", ";
		}
		
		s << v[v.size() - 1];
	}
    return s << "}";
}
 
int main()
{
    // Seed with a real random value, if available
    std::random_device r;
    
    std::vector<std::size_t> seeds; 
 
	for (int ind = 0 ; ind < 8 ; ind++) {
		std::size_t val = r();
		seeds.push_back(val);
		std::cout << std::setw(2) << "ind " << ind 
		          << ", r() "  << std::setw(10) << val
		          << std::endl;
	}
	
	std::cout << seeds << std::endl;
	
	// Normal function termination
	return 0;
}
