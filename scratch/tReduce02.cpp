/**
 * C++ reduce sample code, nontrivial, but not too complex
 * Created on Mon May  6 21:57:03 2024
 *
 * @author: @nnrales, @happy_sisyphus
 *
 * From:
 * - https://stackoverflow.com/questions/40901615/how-to-replicate-map-filter-and-reduce-behaviors-in-c-using-stl
 **/

#include <cmath>     // pow()
#include <vector>
#include <numeric>   // reduce(), accumulate()
#include <execution> // par
#include <algorithm> // transform()
#include <iostream>

#include "insertion_op.hpp"

int main()
{
    std::cout << "1st sample code" << std::endl;

    std::vector<int> in = { 1 , 2 , 3 ,4 };
    std::vector<int> out(in.size());

    std::transform(in.begin() , in.end() , out.begin() , [](const int & val)
    {
        return val+1;
    });

    std::cout << "out\n" << out << std::endl;

    std::vector<int> out2;

    std::transform(in.begin() , in.end() , std::back_inserter(out2) , [](const int & val){
          return val + 1;
    });

    std::cout << "\nout2\n" << out << std::endl;

    // out will be { 2 , 3 ,4 ,5 }


    /// std::transform
    ///
    /// Squaring a vector of integers in place:

    std::vector<int> nums{1,2,3,4};
    auto unary_op = [](int num) {return std::pow(num, 2);};

    std::cout << "\nstd::transform\n"
              << "Squaring a vector of integers in place"
              << std::endl;
    std::cout << "Original:\nnums:\n" << nums << std::endl;
    std::transform(nums.begin(), nums.end(), nums.begin(), unary_op);
    std::cout << "Transformed:\nnums\n" << nums << std::endl;

    // nums: 1, 4, 9, 16


    std::cout << "\nstd::copy_if" << std::endl
              << "Filtering odd numbers only from a vector of integers:"
              << std::endl
    ;

    nums = {1,2,3,4};
    std::vector<int> odd_nums;
    auto pred = [](int num) {return num & 1;};
    std::copy_if(nums.begin(), nums.end(), std::back_inserter(odd_nums), pred);

    std::cout << "odd_nums\n" << odd_nums << std::endl;

    /// odd_nums: 1, 3


    std::cout << "\nstd::reduce" << std::endl
              << "Sum of integers in the vector starting from 0 using parallel execution model."
              << std::endl
    ;

    nums = {1,2,3,4};
    auto binary_op = [](int num1, int num2){return num1 + num2;};
    int result = std::reduce(
        std::execution::par,
        nums.begin(), nums.end(),
        0, binary_op
    );

    std::cout << "result\n" << result << std::endl;

    /// result: 10


    std::cout << "\nstd::accumulate" << std::endl
              << "Same as std::reduce, except only sequential" << std::endl
    ;

    // std::vector<int> nums{1,2,3,4};
    // auto binary_op = [](int num1, int num2){return num1 + num2;};
    result = std::accumulate(nums.begin(), nums.end(), 0, binary_op);

    std::cout << "result\n" << result << std::endl;

    /// Normal function termination
    return 0;
}
