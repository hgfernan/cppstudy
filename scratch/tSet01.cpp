/**
 * Some functions to work with sets
 * Created on Mon Apr 22 19:05:33 2024
 *
 * @author: user
 *
 **/
std::string to_string(std::set<const char*> const &);

std::string to_string(std::set<const char*> const& s)
{
    std:: string result;

    result = "{";
    for (auto it = s.cbegin() ; it != s.cend(); it++) {
        result += std::string(*it) + ", ";
    }

    if (result.length() > 1) {
        result = result.substr(0, result.length() - 2);
    }
    result += "}";

    return result;
}
