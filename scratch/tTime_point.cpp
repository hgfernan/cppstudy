#include <iostream> // cout, endl
#include <chrono>   // class system_clock
#include <iomanip>  // put_time() 

int main() 
{
	const auto now = std::chrono::system_clock::now();
	const auto t_c = std::chrono::system_clock::to_time_t(now);
	std::cout << std::put_time(std::localtime(&t_c), "%F %T.\n");

	return 0;
}

