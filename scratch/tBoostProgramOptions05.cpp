/**
 * Validation of a restricted set of option values
 * Created on Mon Apr 22 15:40:21 2024
 * @author: user
 * From:
 * - https://stackoverflow.com/questions/77695544/use-a-custom-class-to-validate-options-with-boostprogram-options
 * - https://stackoverflow.com/questions/3324285/boostprogram-options-how-to-declare-and-validate-my-own-option-type-when-it
 **/

#include <boost/program_options.hpp>
#include <iostream>
#include <string>

namespace po = boost::program_options;

enum index_methods {
    im_invalid = 0,
    im_classic,
    im_multimap,
    im_first = im_classic,
    im_last = im_multimap
};

std::string to_string(index_methods value)
{
    if (index_methods::im_classic  == value) return "classic";
    if (index_methods::im_multimap == value) return "multimethod";

    return "invalid";
}

struct index_methods_type {

    static
    index_methods from_string(std::string text) {
        static std::map<std::string, index_methods> stoie
        {
            {"classic" , im_classic},
            {"multimap", im_multimap}
        };

        auto it = stoie.find(text);
        if (it == stoie.end()) {
            return index_methods::im_invalid;
        }

        return it->second;
    }

    static bool is_valid(index_methods im)
    {
        return (im != index_methods::im_invalid);
    }

    index_methods im;

    index_methods_type(std::string s) {
        im = index_methods_type::from_string(s);
    }

    index_methods_type(int i) {
        if ((i >= index_methods::im_first) && (i <= index_methods::im_last)) {
            im = static_cast<index_methods>(i);
        } else {
            im = index_methods::im_invalid;
        }
    }
};

std::string to_string(std::vector<std::string> v);

std::string to_string(std::vector<std::string> sv)
{
    std::string result = "[";

    for (std::string s : sv) {
        result += s + ", ";
    }

    if (result.length() > 2) {
        result = result.substr(0, result.length() - 2);
    }

    result += "]";

    return result;
}


void validate(boost::any& v, std::vector<std::string> const& values, index_methods_type*, int) {
    std::vector<std::string> choices;

    for (auto ind = int(index_methods::im_first); ind <= int(index_methods::im_last); ind++) {
        choices.push_back( to_string(static_cast<index_methods>(ind)) );
    }

    namespace pov = po::validators;
    std::cerr << "Validating " << pov::get_single_string(values) << "\n";

    pov::check_first_occurrence(v);
    std::string s = pov::get_single_string(values);

    if (! index_methods_type::is_valid(index_methods_type::from_string(s)) ) {
        std::string msg = "'" + s + "' is not a valid option. "
                          "Please use one of " + to_string(choices);
        throw std::runtime_error(msg);
    }

    // v = s;
    v = boost::any( index_methods_type(lexical_cast<>(s) );
}


int main(int argc, char** argv)
{
    try {
        po::options_description options_description;
        options_description.add_options()
            ("help,h", "Help description")
            ("implementation,i", po::value<char*>()->required(), "Implementation to use");

        po::variables_map vm_;
        store(
            parse_command_line(argc, argv, options_description),
            vm_);

        notify(vm_);

        if (argc == 1 || vm_.count("help")) {
            std::cout << options_description << std::endl;
            std::exit(EXIT_SUCCESS);
        }

    if (vm.count("compression")) {
        cout << "Compression level was set to "
             << vm["compression"].as<double>() << ".\n";
      } else {
            cout << "Compression level was not set.\n";
            }
      }

    } catch (std::exception const& e) {
        std::cerr << "Error: " << e.what() << std::endl;
    };

    return 0;
}

