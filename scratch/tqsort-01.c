#include <stdio.h>   /* printf() */
#include <stdlib.h>  /* qsort() */

#define DIM(x) ( sizeof(x) / sizeof(x[0]) )

struct _OFFICER {
	int id;
	double salary;
};

int	office_comp(const void* l, const void* r) 
{
	struct _OFFICER* lo = (struct _OFFICER*)l;
	struct _OFFICER* ro = (struct _OFFICER*)r;
	
	return lo->id - ro->id;
}

int main() 
{
	int ind = 0;
	int n_officers = 0;
	struct _OFFICER officers[] = {
		{15, 1912.13}, 
		{1, 191200.33}, 
		{10, 19120.23}, 
	};
	
	n_officers = DIM(officers);
	
	qsort(officers, n_officers, sizeof(struct _OFFICER), office_comp);
	
	for (ind = 0; ind < n_officers; ind++) {
		printf("%2d %04d %9.2f\n", ind, officers[ind].id, officers[ind].salary); 
	}
	
	return 0;
}
