/**
 * Some template samples
 * Created on Fri May 10 18:03:16 2024
 *
 * @author: user
 *
 * From:
 * -- https://cplusplus.com/forum/beginner/58978/
 *
 */

#include <list>
#include <deque>
#include <vector>
#include <iostream>
#include <forward_list>

template <template <> class OtherType> class NestedTemplateClass
{
  OtherType<double> cont;

public:
    NestedTemplateClass()
    {
        std::cout << std::boolalpha
                  << "empty " << cont.empty() << std::endl;
    }
};

int main() {
//    std::cout << "\narray template" << std::endl;
//    class NestedTemplateClass<double, std::array> ad;

    std::cout << "\ndeque template" << std::endl;
    class NestedTemplateClass<double, std::deque> dd;

    std::cout << "\nforward_list template" << std::endl;
    class NestedTemplateClass<double, std::forward_list> fd;

    std::cout << "\nlist template" << std::endl;
    class NestedTemplateClass<double, std::list> ld;

    std::cout << "\nvector template" << std::endl;
    class NestedTemplateClass<double, std::vector> vd;

    /// Normal function termination
    return 0;
}


