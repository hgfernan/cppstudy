/**
 * Use of chrono library to time a process
 * Created on Mon Apr 15 14:31:24 2024
 *
 * @author: o11c
 * From:
 * - https://stackoverflow.com/questions/15092504/how-to-time-a-function-in-milliseconds-without-boosttimer
 *
 **/

#include <chrono> /// steady_clock() time_point() duration_cast...
#include <thread> /// sleep()
#include <iostream>

// There are other clocks, but this is usually the one you want.
// It corresponds to CLOCK_MONOTONIC at the syscall level.
using Clock = std::chrono::steady_clock;
using std::chrono::time_point;
using std::chrono::duration_cast;
using std::chrono::milliseconds;
using namespace std::literals::chrono_literals;
using std::this_thread::sleep_for;

int main()
{
    time_point<Clock> start = Clock::now();
    sleep_for(500ms);
    time_point<Clock> end = Clock::now();
    milliseconds diff = duration_cast<milliseconds>(end - start);
    std::cout << diff.count() << "ms" << std::endl;
}
