#include <stdio.h>

#define ON_INPUT "input"
#define OS_INPUT "i"

#define OJ_INPUT ON_INPUT "," OS_INPUT

int main()
{
    printf("Option  long   '%s'\n", ON_INPUT);
    printf("Option  short  '%s'\n", OS_INPUT);
    printf("Options joined '%s'\n", OJ_INPUT);
    
    return 0;
}
