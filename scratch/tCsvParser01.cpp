/**
 * Reading and writing CSV files using @vincentlaucsb csv-parser library
 * Created on Wed May  8 15:48:19 2024
 *
 * @author: @vincentlaucsb
 *
 * From:
 * - https://github.com/vincentlaucsb/csv-parser/blob/master/programs/round_trip.cpp
 *
 */
#include <iostream>

#include <csv.hpp>

int main(int argc, char* const* argv) {
    if (argc < 3) {
        std::cout << "Usage: " << argv[0] << " [file] [out]" << std::endl;
        exit(1);
    }

    std::string file = argv[1];
    std::string out = argv[2];

    std::ofstream outfile(out);
    auto writer = csv::make_csv_writer(outfile);

    csv::CSVFormat format;
    format.variable_columns(true);
    csv::CSVReader reader(file, format);
    writer << reader.get_col_names();

    for (auto& row: reader) {
        writer << std::vector<std::string>(row);
    }

    return 0;
}

