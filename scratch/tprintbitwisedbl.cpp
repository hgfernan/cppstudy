/**
 * Sample code to print an address in C++
 * Created on Thu Jan  2 01:08:49 2025
 * 
 * @author: hilton
 * 
 **/


#include <iostream> // cout, hex, endl
#include <cstring>  // memcpy() 


int main(void) 
{
    long unsigned int lui = 0; 
    double d = 1.234567e89; 
    
    memcpy(&lui, reinterpret_cast<long unsigned int*>(&d), sizeof(double)); 
    
    std::cout << "0x" << std::hex 
		<< lui << std::endl;
}
