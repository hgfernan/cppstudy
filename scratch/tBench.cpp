/**
 * Testbed for a timing benchmark
 * Created on Fri May 10 14:35:45 2024
 *
 * @author: user
 *
 */

#include <cmath>    /// sqrt()
#include <chrono>   /// steady_clock() time_point() duration_cast...
#include <iomanip>  /// put_time()
#include <random>   /// random_device, default_random_device...
#include <numeric>  /// reduce(), accumulate()
#include <iostream>
#include <filesystem>  /// class path
#include <functional>  /// class template function<>
#include <string_view> /// std::string replacement string_view


#include <csv.hpp> /// csv

struct {
};

template <typename Kontainer, typename ValueType>
void presentation(std::string_view label,
                   std::size_t      ntimes,
                   std::function<double(Kontainer<ValueType>)> microbench,
                   Kontainer<ValueType>& data,
                   csv::DelimWriter writer
                  )
{
    const auto now = std::chrono::system_clock::now();
    const auto t_c = std::chrono::system_clock::to_time_t(now);

    /// Start timer for the whole calculation

    std::cout << "\n"
              << std::put_time(std::localtime(&t_c), "%F %T: ")
              << label << " to sum "
              << data.size() << " numbers"
              << std::endl
    ;

    auto rv = stat_lab(ntimes, microbench, data);
    std::cout << "\tResult " << rv
              << std::endl
    ;

    /// Stop timer for the whole calculation

    /// Normal function termination
    return;
}

