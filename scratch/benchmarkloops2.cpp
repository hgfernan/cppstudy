/**
 * Benchmarking several C++ loops, using std::list<>
 * Created on Tue May  7 21:11:14 2024
 *
 * @author: user
 *
 * From:
 * - https://stackoverflow.com/questions/74413185/how-to-correctly-pass-a-function-with-parameters-to-another-function
 * - https://stackoverflow.com/questions/57538507/how-to-convert-stdchronoduration-to-double-seconds
 */

#include <cmath>    /// sqrt()
#include <chrono>   /// steady_clock() time_point() duration_cast...
#include <iomanip>  /// put_time()
#include <random>   /// random_device, default_random_device...
#include <list>
#include <thread>   /// sleep()
#include <numeric>  /// reduce(), accumulate()
#include <iostream>
#include <filesystem>  /// class path
#include <functional>  /// class template function<>
#include <string_view> /// std::string replacement string_view


#include <csv.hpp> /// csv

// There are other clocks, but this is usually the one you want.
// It corresponds to CLOCK_MONOTONIC at the syscall level.
using Clock = std::chrono::steady_clock;
using std::chrono::time_point;
using std::chrono::duration;
using std::chrono::duration_cast;
using std::chrono::milliseconds;
using namespace std::literals::chrono_literals;
using std::this_thread::sleep_for;

double
sum_for(const std::list<double>& data)
{
    double result = 0.0;
    std::size_t nitems = data.size();

    // std::cout << "nitems " << nitems << std::endl;

    auto it = data.cbegin();
    for (std::size_t ind = 0; ind < nitems; ind++) {
        result += *it;
        it++;
        // std::cout << "ind "      << ind
        //           << ", result " << result
        //           << std::endl
        // ;
    }

    /// Normal function termination
    return result;
}

double
sum_forsz(const std::list<double>& data)
{
    double result = 0.0;

    auto it = data.cbegin();
    for (std::size_t ind = 0; ind < data.size(); ind++) {
        result += *it;
        it++;
    }

    /// Normal function termination
    return result;
}

double
sum_rangefor(const std::list<double>& data)
{
    double result = 0;

    for (auto d : data) {
        result += d;
    }

    /// Normal function termination
    return result;
}

double
sum_while(const std::list<double>& data)
{
    double result = 0;
    std::size_t ind    = 0;
    std::size_t nitems = data.size();

    auto it = data.cbegin();
    while (ind < nitems) {
        result += *it;
        ind++;
        it++;
    }

    /// Normal function termination
    return result;
}

double
sum_whilesz(const std::list<double>& data)
{
    double result = 0;
    std::size_t ind    = 0;

    auto it = data.cbegin();
    while (ind < data.size()) {
        result += *it;
        ind++;
        it++;
    }

    /// Normal function termination
    return result;
}

double
sum_accumulate(const std::list<double>& data)
{
    auto result = std::accumulate(data.cbegin(), data.cend(), 0.0);

    /// Normal function termination
    return result;
}


double
sum_reduce(const std::list<double>& data)
{
    auto result = std::reduce(data.cbegin(), data.cend(), 0.0);

    /// Normal function termination
    return result;
}


std::size_t
stat_lab(std::size_t ntimes,
         std::function<double(std::list<double>)> func,
         std::list<double> data)
{
    time_point<Clock> task_start;
    time_point<Clock> task_finish;
    double diff;

    time_point<Clock> start;
    time_point<Clock> finish;

    double avg = 0.0;
    double var = 0.0;
    double stddev = 0.0;
    double result = 0.0;

    task_start = Clock::now();

    for (size_t count = 0; count < ntimes; count++) {
        start = Clock::now();

        double rv = func(data);

        finish = Clock::now();
        diff   = duration<double>(finish - start).count();

        avg += diff;
        var += diff * diff;

        result += rv;
    }

    task_finish = Clock::now();
    diff        = duration<double>(task_finish - task_start).count();

    avg /= ntimes;
    var = var - ntimes*avg*avg;
    var /= ntimes - 1;
    stddev = std::sqrt(var);

    std::cout << "Elapsed time (seconds)   " << diff        << std::endl
              << "Number of measurements   " << ntimes      << std::endl
              << "Data size                " << data.size() << std::endl
              << "Mean average             " << avg         << std::endl
              << "Variance                 " << var         << std::endl
              << "Standard deviation       " << stddev      << std::endl
              << "Coefficient of variation " << (100.0 * stddev / avg) << std::endl
    ;

    result /= ntimes;

    /// Normal function termination
    return result;
}


void presentation(std::string_view label,
                  std::size_t      ntimes,
                  std::function<double(std::list<double>)> func,
                  std::list<double>& data,
                  csv::DelimWriter writer
                 )
{
    const auto now = std::chrono::system_clock::now();
    const auto t_c = std::chrono::system_clock::to_time_t(now);

    /// Start timer for the whole calculation

    std::cout << "\n"
              << std::put_time(std::localtime(&t_c), "%F %T: ")
              << label << " to sum "
              << data.size() << " numbers"
              << std::endl
    ;

    auto rv = stat_lab(ntimes, func, data);
    std::cout << "\tResult " << rv
              << std::endl
    ;

    /// Stop timer for the whole calculation

    /// Normal function termination
    return;
}


int main(int argc, char* const* argv)
{
    const std::size_t ntimes = 1000;
    const std::size_t ndata = 1000 * 1000;
    std::list<double> data;

    if (argc < 2) {
        std::cout << argv[0] << ": *** ERROR *** "
                  << "Missing output filename"
                  << std::endl
        ;
        std::cout << "\nUsage:" << std::endl;
        std::cout << std::filesystem::path(argv[0]).filename()
                  << " OUTPUT_CSV_NAME"
                  << std::endl;

        return 1;
    }

    std::vector<std::string>
        header{"Seq", "DtStart", "DtFinish", "Label", "NTimes",
               "NData", "OpResult", "Avg", "Var", "StDev", "CoV"};
    std::ofstream outfile(argv[1]);
    auto writer = csv::make_csv_writer(outfile);
    writer << header;

    time_point<Clock> proc_start;
    time_point<Clock> proc_finish;
    double diff;

    /// \todo generate the data
    std::seed_seq seed{3841299750ul,  647485403ul, 1492458493ul,
                       1211501403ul, 2007303644ul, 3678919580ul,
                       3346392214ul,   68269330ul};
    // Generate a normal distribution around that mean
    std::mt19937 e2(seed);
    std::normal_distribution normal_dist(0.0, 1e5);

    for (std::size_t count = 0; count < ndata; count++) {
        data.push_back(normal_dist(e2));
    }

    proc_start = Clock::now();

    presentation("For loop",                   ntimes, sum_for, data);
/*
    presentation("For loop consulting size",   ntimes, sum_forsz, data);
    presentation("For loop using range",       ntimes, sum_rangefor, data);
    presentation("While loop",                 ntimes, sum_while, data);
    presentation("While loop consulting size", ntimes, sum_whilesz, data);
    presentation("Accumulate"                , ntimes, sum_accumulate, data);
    presentation("Reduce"                    , ntimes, sum_reduce, data);
*/
    proc_finish = Clock::now();
    diff        = duration<double>(proc_finish - proc_start).count();
    std::cout << "Total elapsed time (seconds) " << diff << std::endl;

    /// Normal function termination
    return 0;
}
