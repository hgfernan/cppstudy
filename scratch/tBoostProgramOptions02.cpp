/** 
 * How to use \c boost \c program_options library
 * Testbed for \c OrderGen
 * Created on Tue Apr 16 06:17:16 2024
 * 
 * @author: user
 * From: Luke Shirley & systemcpro
 * - https://stargckoverflow.com/questions/45466939/parsing-command-line-arguments-with-boost-program-options-c
 *
 **/

#include <iostream>
// #include <iterator>
#include <boost/program_options.hpp>
#include <exception>

namespace po = boost::program_options;

int main(int argc, char* const* argv){
    try {
        std::size_t nSecurities;
        std::size_t nOrdersPerSec;
        std::size_t amount;
        double fragmentFlut;

        /// Option names 
        std::string onSecurities = "securities";
        std::string onOrders     = "orders";
        std::string onAmount     = "amount";
        std::string onFlutuation = "flutuation";

        /// Option short names 
        std::string osSecurities = "s";
        std::string osOrders     = "o";
        std::string osAmount     = "a";
        std::string osFlutuation = "f";
        
        /// Option descriptions 
        std::string odSecurities = "Number of securities";
        std::string odOrders     = "Number of orders per security";
        std::string odAmount     = "Amount per security";
        std::string odFlutuation = "Percent of fragment variation";
        
        po::options_description desc("Allowed options");
        desc.add_options()
            ("help,h", "produce help message")
            ("securities,s", 
                po::value<std::size_t>(), 
                "Number of securities")
            ("orders,o", 
                po::value<std::size_t>(), 
                "Number of orders per security")
            ("amount,a", 
                po::value<std::size_t>(), 
                "Amount per security")
            ("flutuation,f", 
                po::value<double>(),
                "Percent of fragment variation");

        po::variables_map vm;
        po::store(po::parse_command_line(argc, argv, desc), vm);
        po::notify(vm);

        if (vm.count("help")) {
            std::cout << desc << "\n";
            return 0;
          }

        std::vector<std::string> 
            opt_names{onSecurities, onOrders, onAmount, onFlutuation};
        
        bool missing_option = false;
        for (auto opt_name : opt_names) {
            if (! vm.count(opt_name)) {
                std::cerr << "ERROR " << opt_name 
                          << " was not informed." << std::endl;
                missing_option = true;
            } 
        }
        
        if (missing_option) {
            std::cout << "ERROR All options should be informed"
                      << std::endl;
                      
            return 1;
        }
        
        nSecurities   = vm[onSecurities].as<std::size_t>();
        nOrdersPerSec = vm[onOrders].as<std::size_t>();
        amount        = vm[onAmount].as<std::size_t>();
        fragmentFlut  = vm[onFlutuation].as<double>();
        
        std::cout << odSecurities + ": " << nSecurities   << std::endl;
        std::cout << odOrders + ": " << nOrdersPerSec << std::endl;
        std::cout << odAmount + ": " << amount << std::endl;
        std::cout << odFlutuation + ": " << fragmentFlut << std::endl;
        
    }
    catch(std::exception& e) {
        std::cerr << "error: " << e.what() << "\n";
        return 2;
    }
    catch(...) {
        std::cerr << "Exception of unknown type!\n";
        return 3;
    }
    
    return 0;
}
