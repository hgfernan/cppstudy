/**
 * Validate the presence of a single regular file, given as a path
 * Created on Mon Apr 22 14:51:47 2024
 * @author: @sehe
 *
 * From:
 * - https://stackoverflow.com/questions/77695544/use-a-custom-class-to-validate-options-with-boostprogram-options
 **/
#include <boost/program_options.hpp>
#include <filesystem>
#include <iostream>

namespace po = boost::program_options;

namespace boost {
    void validate(boost::any& v, std::vector<std::string> const& values, std::filesystem::path*, int) {
        namespace pov = po::validators;
        std::cerr << "Validating " << pov::get_single_string(values) << "\n";

        pov::check_first_occurrence(v);
        std::filesystem::path p(pov::get_single_string(values));
        if (!p.is_relative())
            throw std::runtime_error("path must be relative");
        if (!is_regular_file(p))
            throw std::runtime_error("path must be an existing regular file");
        v = p;
    }
} // namespace boost

int main(int argc, char** argv) try {
    po::options_description options_description;
    options_description.add_options()("help", "") //
        ("path", po::value<std::filesystem::path>()->required(), "");

    std::cout << "Current path is " << std::filesystem::current_path() << '\n'; // (1)
    po::variables_map vm_;
    store(
        parse_command_line(argc, argv, options_description),
        vm_);

    if (argc == 1 || vm_.count("help")) {
        std::cout << options_description << std::endl;
        std::exit(EXIT_SUCCESS);
    }

    notify(vm_);
} catch (std::exception const& e) {
    std::cerr << "Error: " << e.what() << std::endl;
}
