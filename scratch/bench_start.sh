#! /bin/bash

# TODO automatic increment of filename suffix to avoid overwriting

for count in {1..100} ; do 
	echo $( date +"%Y-%d-%m %H:%M:%S" ) Iteration "${count}" | tee benchmarkloops-1000-3.out 
	./Benchmark\ Loops | tee -a benchmarkloops-1000-3.out 
done
