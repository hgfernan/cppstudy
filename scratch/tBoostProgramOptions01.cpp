/** 
 * How to use \c boost \c program_options library
 * Created on Tue Apr 16 04:35:14 2024
 * 
 * @author: user
 * From: Luke Shirley & systemcpro
 * - https://stackoverflow.com/questions/45466939/parsing-command-line-arguments-with-boost-program-options-c
 *
 **/

#include <iostream>
#include <iterator>
#include <boost/program_options.hpp>
#include <exception>

using std::cerr;
using std::cout;
using std::endl;
using std::exception;
namespace po = boost::program_options;

int main(int ac, char** av){

try {

    po::options_description desc("Allowed options");
    desc.add_options()
        ("help,h", "produce help message")
        ("compression,c", po::value<double>(), "set compression level");

    po::variables_map vm;
    po::store(po::parse_command_line(ac, av, desc), vm);
    po::notify(vm);

    if (vm.count("help")) {
        cout << desc << "\n";
        return 0;
      }

    if (vm.count("compression")) {
        cout << "Compression level was set to "
             << vm["compression"].as<double>() << ".\n";
      } else {
            cout << "Compression level was not set.\n";
            }
      }
    catch(exception& e) {
        cerr << "error: " << e.what() << "\n";
        return 1;
    }
    catch(...) {
        cerr << "Exception of unknown type!\n";
    return 1;
    }
return 0;
}
