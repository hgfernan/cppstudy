/**
 * ***** NOT WORKING ***** 
 * Testing/learning C regex -- recognizing a number
 * Created on Sat Dec 28 23:23:13 2024
 * 
 * @author: By Sarveshwar Shukla
 * From: 
 * -- https://www.scaler.com/topics/c-regex/
 **/


#include <stdio.h>
#include <regex.h>

// Function to inform whether the pattern is found or not
void displayPattern(int value)
{
    if (value == 0)
    {
        printf("Pattern matched\n");
    }
    else if (value == REG_NOMATCH)
    {
        printf("Pattern not found\n");
    }
    else
    {
        printf("Some error occured\n");
    }
}

int main()
{
     // declaring a variable to create a regex
    regex_t rx;

    // creating three int variables to perform the operation
    int rv = 0;
    int d1 = 0;
    int d2 = 0;
    int d3 = 0;

    // creating a regex and then comparing the pattern with the target string
    // rv = regcomp(&rx, "[-+]?([:digit:]*)\\.?([:digit:]+)?([+-]?[eE][:digit:]+)?", 0);
    rv = regcomp(&rx, "[0-9]+", 0);
    printf("return value from compilation: %d\n", rv);
    
    d1 = regexec(&rx, "24", 0, NULL, 0);
    d2 = regexec(&rx, "24.0", 0, NULL, 0);
    d3 = regexec(&rx, "-24.0", 0, NULL, 0);

    // calling the displayPattern() function to output the result of the above operation
    displayPattern(d1);
    displayPattern(d2);
    displayPattern(d3);

    return 0;
}
