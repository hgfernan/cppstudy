/**
 * Testbed for std::sprintf
 * Created on Sun Apr 14 22:39:34 2024
 * 
 * @author: user
 * 
 **/

#include <cstdio> // sprintf()
#include <string>  
#include <iostream>

int main() 
{   
    std::string s(30, '\0');    
    std::cout << "Before reserve: "
              << "s.size " << s.size() 
              << ", s.capacity " << s.capacity() << std::endl;
              
    s.reserve(100);
    std::cout << "After  reserve(): "
              << "s.size " << s.size() 
              << ", s.capacity " << s.capacity() << std::endl;
              
    std::sprintf(s.data(), "SecurityId%06lu", 10ul);
    
    std::cout << "s '" << s << "'" << std::endl;
    std::cout << "After  sprintf(): "
              << "s.size " << s.size() 
              << ", s.capacity " << s.capacity() << std::endl;
    
    return 0;
}
