/**
 * Is HUGE_VAL available ?
 * Created on Mon Dec 30 07:29:07 2024
 * 
 * @author: CppReference.com authors
 * From: 
 * -- https://en.cppreference.com/w/c/numeric/math/HUGE_VAL
 * 
 * OBS: HUGE_VAL is printed as `inf` in gcc 12.2.0
 **/

#include <math.h>  
#include <stdio.h>
 
int main(void)
{
    const double result = 1.0 / 0.0;
    printf("1.0/0.0 == %f\n", result);
    if (result == HUGE_VAL)
        puts("1.0/0.0 == HUGE_VAL");
    
    return 0;
}
