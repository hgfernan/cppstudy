#include <stdio.h> 
#include <math.h>

typedef double (*cmp) (double, double);

void test(cmp func, double left, double right)
{
	double extremum = func(left, right);
	printf("Is %f smaller than'%f ? ", extremum, right);
	puts((extremum < right) ? "No": "Yes"); 
}

int main() 
{
	cmp func;
	double left = 10, right = 15;

	puts("fmin");
	func = fmin;
	test(func, left, right);

	puts("fmax");
	func = fmax;
	test(func, left, right);
	
	return 0;
}

