/**
 * Adapting template samples to benchmarking
 * Created on Fri May 10 19:39:57 2024
 *
 * @author: user
 *
 * From:
 * -- https://cplusplus.com/forum/beginner/58978/
 *
 */

#include <cmath>

#include <list>
#include <deque>
#include <vector>
#include <iostream>
#include <functional>  /// class template function<>
#include <forward_list>


template <typename DataType, template <class> class Container> class Microbenchmark
{
    Container<DataType> cont;

public:
    Microbenchmark( const std::size_t& ntimes,
                    const std::size_t& ndata,
                    std::function<double(Container<DataType>)> f
                  )
    {
        std::cout << std::boolalpha
                  << "empty " << cont.empty() << std::endl;
    }
};

double for_sum(std::deque<double> data)
{
    return 0.0;
}

double func(double d)
{
    return 0.0;
}

int main() {
//    std::cout << "\narray template" << std::endl;
//    class NestedTemplateClass<double, std::array> ad;

    const std::size_t ntimes = 100;
    const std::size_t ndata = (100 * 100);

    std::cout << "\ndeque template" << std::endl;
    class Microbenchmark<double, std::deque> dd(ntimes, ndata, for_sum);

/*
    std::cout << "\nforward_list template" << std::endl;
    class NestedTemplateClass<double, std::forward_list> fd;

    std::cout << "\nlist template" << std::endl;
    class NestedTemplateClass<double, std::list> ld;

    std::cout << "\nvector template" << std::endl;
    class NestedTemplateClass<double, std::vector> vd;
*/
    /// Normal function termination
    return 0;
}


