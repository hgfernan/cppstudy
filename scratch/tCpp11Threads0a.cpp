/*
 * Use of pointers a thread's worker function 
 *
 * Created on Sat Jan 21 22:26:40 2023
 * @author: hilton
 * From: 
 * * https://en.cppreference.com/w/cpp/thread/thread/thread 
 */

#include <iostream> // std::cout, std::endl
#include <thread>   // std::thread()


void simpleinc(int* pi)
{ 
    std::cout << "In the thread:  The address is " << pi << std::endl;
    ++(*pi);  
}

int main()
{
    int n = 0;
    std::cout << "In the program: The address is " << (&n) << std::endl;
    std::thread th(simpleinc, &n);
    th.join();

    std::cout << "The result is " << n << std::endl;

    return n;
}
