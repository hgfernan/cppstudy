/**
 * Testbed for string split
 * Created on Sat Apr 13 22:40:08 2024
 * 
 * @author: Arafat Hasan
 * From: 
 * - https://stackoverflow.com/questions/14265581/parse-split-a-string-in-c-using-string-delimiter-standard-c
 */

#include <iostream>
#include <sstream>
#include <vector>

// for string delimiter
std::vector<std::string> split(std::string s, std::string delimiter) {
    size_t pos_start = 0, pos_end, delim_len = delimiter.length();	
    std::string token;
    std::vector<std::string> res;

    while ((pos_end = s.find(delimiter, pos_start)) != std::string::npos) {
        token = s.substr (pos_start, pos_end - pos_start);
        pos_start = pos_end + delim_len;
        res.push_back (token);
    }

    res.push_back (s.substr (pos_start));
    return res;
}

int main() {
    std::string delimiter = "-+";
    
    std::string str1 = "adsf-+qwret-+nvfkbdsj-+orthdfjgh-+dfjrleih";        
    std::vector<std::string> v = split (str1, delimiter);    
    std::cout << "Original string: \"" << str1 << "\"" << std::endl;
    
    for (auto i : v) {
		std::cout << i << std::endl;
	}

    std::string str2 = "adsf-+qwret-+nvfkbdsj-+orthdfjgh-+dfjrleih-+";    
    v = split (str1, delimiter);    
    std::cout << "\nOriginal string: \"" << str2 << "\"" << std::endl;
    
    for (auto i : v) {
		std::cout << i << std::endl;
	}

    return 0;
}
