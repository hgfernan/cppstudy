/**
 * Sample code to use
 * Created on Mon Apr 22 21:38:34 2024
 * @author: user
 *
 * From:
 * - https://www.boost.org/doc/libs/1_85_0/doc/html/program_options/howto.html
 *      By Vladimir Prus
 **/

// Copyright Vladimir Prus 2002-2004.
// Distributed under the Boost Software License, Version 1.0.
// (See accompanying file LICENSE_1_0.txt
// or copy at http://www.boost.org/LICENSE_1_0.txt)

// This example shows how a user-defined class can be parsed using
// specific mechanism -- not the iostream operations used by default.
//

#include <boost/program_options.hpp>

using namespace boost;
using namespace boost::program_options;

#include <string>
#include <vector>
#include <numeric>   /// reduce()
#include <iostream>
#include <algorithm> /// find()

using namespace std;

const std::vector<std::string>
    by_number_codenames{"ordered_map", "multimap"};

class by_number {
    int m_code;
public:
    by_number(int n) : m_code(n) {}
    int code() const {return m_code;}
    static int calc_code(const std::string& text);
    static std::string get_codenames();
    static std::string get_codename(int code);
};

int by_number::calc_code(const std::string& text)
{
    auto rv = std::find(by_number_codenames.cbegin(),
                        by_number_codenames.cend(),
                        text);

    if (by_number_codenames.cend() == rv) return -1;

    int diff = (rv - by_number_codenames.cbegin());

    // Return to indicate failure
    return diff;
}

std::string by_number::get_codenames()
{
    std::string result;

    result = std::reduce(by_number_codenames.cbegin(),
                         by_number_codenames.cend(),
                         std::string(),
                         [] (std::string previousResult, std::string item){
                             if (previousResult.empty())
                                return item;
                             return previousResult + ", " + item;
                         });
    result = "[" + result + "]";

    /// Normal function termination
    return result;
}

std::string by_number::get_codename(int code)
{
    if ( (code < 0) || ( code >= int(by_number_codenames.size()) ))
        /// Return to indicate failure
        return "Unknown";

    /// Normal function termination
    return by_number_codenames[code];
}

/// Overload the 'validate' function for the user-defined class.
void validate(boost::any& v,
              const std::vector<std::string>& values,
              by_number*, int)
{
    using namespace boost::program_options;

    // Make sure no previous assignment to 'a' was made.
    validators::check_first_occurrence(v);
    // Extract the first string from 'values'. If there is more than
    // one string, it's an error, and exception will be thrown.
    const string& s = validators::get_single_string(values);

    int rv = by_number::calc_code(s);

    if (0 < rv) {
        v = any( by_number(lexical_cast<int>(rv)) );
    } else {
        throw validation_error(validation_error::invalid_option_value);
    }
}


int main(int argc, char* const* argv)
{
    std::string help_msg =
        "codename (one of " + by_number::get_codenames() + ")";

    try {
        options_description desc("Allowed options");
        desc.add_options()
            ("help", "produce a help screen")
            ("version,v", "print the version number")
            ("code,c", value<by_number>(),
                 help_msg.c_str())
            ;

        variables_map vm;
        store(parse_command_line(argc, argv, desc), vm);

        if ((1 == argc) || vm.count("help")) {
            cout << "Usage: regex [options]\n";
            cout << desc;
            return 0;
        }
        if (vm.count("version")) {
            cout << "Version 1.\n";
            return 0;
        }
        if (vm.count("code")) {
            int rv = vm["code"].as<by_number>().code();
            cout << "The code is " << rv
                 << " (\"" << by_number::get_codename(rv) << "\")"
                 << std::endl;
        }
    }
    catch(std::exception& e)
    {
        cout << e.what() << "\n";

        /// Return to indicate failure
        return 1;
    }

    /// Normal function termination
    return 0;
}
