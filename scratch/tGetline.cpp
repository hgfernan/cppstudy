/** 
 * Use getline() to read input until EOF is reached. 
 * Created on Tue Apr 16 03:18:43 2024
 * 
 * @author: geeksforgeeks.org
 * From:
 * - https://www.geeksforgeeks.org/read-input-until-eof-in-cpp/
**/

// C++ program to use getline() to read input until EOF is 
// reached. 
  
#include <fstream> 
#include <iostream> 
#include <string> 
  
using namespace std; 
  
int main() 
{ 
    // Specify filename to read (replace with your actual 
    // file) 
    std::string filename = "input.txt"; 
  
    // Open file for reading 
    std::ifstream inputFile(filename); 
  
    // Check file open success 
    if (!inputFile.is_open()) { 
        std::cerr << "Error opening file: " << filename << std::endl; 
        return 1; 
    } 
  
    // String to store each line 
    std::string line; 
  
    // Read each line until EOF 
    std::cout << "The file contents are: \n"; 
    while (std::getline(inputFile, line)) { 
        // Process each line (replace with your logic) 
        std::cout << line << std::endl; 
    } 
  
    // Close file when done 
    inputFile.close(); 
  
    return 0; 
}
