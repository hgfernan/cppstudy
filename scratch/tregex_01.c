/**
 * Testing/learning C regex
 * Created on Sat Dec 28 23:18:30 2024
 * 
 * @author: By Sarveshwar Shukla
 * From: 
 * -- https://www.scaler.com/topics/c-regex/
 **/


#include <stdio.h>
#include <regex.h>

// Function to inform whether the pattern is found or not
void displayPattern(int value)
{
    if (value == 0)
    {
        printf("Pattern matched\n");
    }
    else if (value == REG_NOMATCH)
    {
        printf("Pattern not found\n");
    }
    else
    {
        printf("Some error occured\n");
    }
}

int main()
{
     // declaring a variable to create a regex
    regex_t rx;

    // creating three int variables to perform the operation
    int d1, d2, d3;

    // creating a regex and then comparing the pattern with the target string
    d1 = regcomp(&rx, "Hello World", 0);
    d1 = regexec(&rx, "Hello World again", 0, NULL, 0);

   // creating another regex and then comparing the pattern with the target string
    d2 = regcomp(&rx, "Hello World", 0);
    d2 = regexec(&rx, "Hello World once again", 0, NULL, 0);

   // creating a regex one more time and then comparing the pattern with the target string
    d3 = regcomp(&rx, "Hello World", 0);
    d3 = regexec(&rx, "Hello again", 0, NULL, 0);

    // calling the displayPattern() function to output the result of the above operation
    displayPattern(d1);
    displayPattern(d2);
    displayPattern(d3);

    return 0;
}
