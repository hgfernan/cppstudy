/**
 * Simple macros to present JSON data
 * Created on Wed Feb  5 21:58:02 2025
 * 
 * @author: hilton
 * 
 */
 
#if !defined(JSON_MACROS_H) 
#define JSON_MACROS_H

#include <stdio.h>   /* fputs(), printf() */
#include <string.h>  /* strcat() */
#include <stdbool.h> /* bool, false, true */

#define JSON_MACROS_VARS \
int _jm_depth = -1;\
bool _jm_has_sibbling[128] = {false};\
char _jm_prefix[128] = "";

#define JM_OPEN_ALL(f) {\
	fputs("{\n", f);\
	_jm_depth++;\
	strcat(_jm_prefix, "\t");\
}

#define JM_CLOSE_ALL(f) {\
	fputs("\n}\n", f);\
	_jm_depth = -1;\
	_jm_prefix[0] = '\0';\
}

#define JM_OPEN_STRUCT(name, f) {\
	if (_jm_has_sibbling[_jm_depth]) fputs(",\n", f);\
	fprintf(f, "%s\"%s\": {\n", _jm_prefix, name);\
	_jm_has_sibbling[_jm_depth] = true;\
	_jm_depth++;\
	strcat(_jm_prefix, "\t");\
}

#define JM_CLOSE_STRUCT(f) {\
	if (_jm_has_sibbling[_jm_depth]) fputs("\n", f);\
	_jm_has_sibbling[_jm_depth] = false;\
	_jm_prefix[_jm_depth] = '\0';\
	fprintf(f, "%s}", _jm_prefix);\
	_jm_depth--;\
}

#define JM_ADD_PAIR_STR(key, value, f) {\
	if (_jm_has_sibbling[_jm_depth]) fputs(",\n", f);\
	_jm_has_sibbling[_jm_depth] = true;\
	fprintf(f, "%s\"%s\": \"%s\"", _jm_prefix, key, value);\
}

#define JM_ADD_PAIR_INT(key, value, f) {\
	if (_jm_has_sibbling[_jm_depth]) fputs(",\n", f);\
	_jm_has_sibbling[_jm_depth] = true;\
	fprintf(f, "%s\"%s\": %d", _jm_prefix, key, value);\
}

#define JM_ADD_PAIR_UINT(key, value, f) {\
	if (_jm_has_sibbling[_jm_depth]) fputs(",\n", f);\
	_jm_has_sibbling[_jm_depth] = true;\
	fprintf(f, "%s\"%s\": %lu", _jm_prefix, key, value);\
}

#define JM_ADD_PAIR_DBL(key, value, f) {\
	if (_jm_has_sibbling[_jm_depth]) fputs(",\n", f);\
	_jm_has_sibbling[_jm_depth] = true;\
	fprintf(f, "%s\"%s\": %g", _jm_prefix, key, value);\
}

#define JM_ADD_PAIR_BOOL(key, value, f) {\
	if (_jm_has_sibbling[_jm_depth]) fputs(",\n", f);\
	_jm_has_sibbling[_jm_depth] = true;\
	fprintf(f, "%s\"%s\": %s", _jm_prefix, key,\
		value? "true", "false");\
}

#define JM_ADD_PAIR_CHAR(key, value, f) {\
	if (_jm_has_sibbling[_jm_depth]) fputs(",\n", f);\
	_jm_has_sibbling[_jm_depth] = true;\
	fprintf(f, "%s\"%s\": '%c'", _jm_prefix, key, value);\
}

#define JM_OPEN_ARRAY(name, f) {\
	if (_jm_has_sibbling[_jm_depth]) fputs(",\n", f);\
	fprintf(f, "%s\"%s\": [\n", _jm_prefix, name);\
	_jm_has_sibbling[_jm_depth] = true;\
	_jm_depth++;\
	strcat(_jm_prefix, "\t");\
}

#define JM_CLOSE_ARRAY(f) {\
	if (_jm_has_sibbling[_jm_depth]) fputs("\n", f);\
	_jm_has_sibbling[_jm_depth] = false;\
	_jm_prefix[_jm_depth] = '\0';\
	fprintf(f, "%s]", _jm_prefix);\
	_jm_depth--;\
}

#define JM_ADD_ELMT_STR(value, f) {\
	if (_jm_has_sibbling[_jm_depth]) fputs(",\n", f);\
	_jm_has_sibbling[_jm_depth] = true;\
	fprintf(f, "%s\"%s\"", _jm_prefix, value);\
}

#define JM_ADD_ELMT_UINT(value, f) {\
	if (_jm_has_sibbling[_jm_depth]) fputs(",\n", f);\
	_jm_has_sibbling[_jm_depth] = true;\
	fprintf(f, "%s%lu", _jm_prefix, value);\
}

#define JM_ADD_ELMT_DBL(value, f) {\
	if (_jm_has_sibbling[_jm_depth]) fputs(",\n", f);\
	_jm_has_sibbling[_jm_depth] = true;\
	fprintf(f, "%s%g", _jm_prefix, value);\
}

#define JM_ADD_ELMT_INT(value, f) {\
	if (_jm_has_sibbling[_jm_depth]) fputs(",\n", f);\
	_jm_has_sibbling[_jm_depth] = true;\
	fprintf(f, "%s%d", _jm_prefix, value);\
}

#define JM_ADD_ELMT_BOOL(value, f) {\
	if (_jm_has_sibbling[_jm_depth]) fputs(",\n", f);\
	_jm_has_sibbling[_jm_depth] = true;\
	fprintf(f, "%s%s", _jm_prefix, value? "true", "false");\
}

#endif /* !defined(JSON_MACROS_H) */
