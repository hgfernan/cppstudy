/*
 * Simple use of std::async and futures
 *
 * Created on Sat Jan 21 22:12:03 2023
 * @author: @sehe from StackOverflow.com
 * From: 
 * * https://stackoverflow.com/questions/7686939/c-simple-return-value-from-stdthread 
 */

#include <iostream> // std::cout, std::endl
#include <thread>   // std::async()
#include <future>   // std::promise<T>


int simplefunc(std::string a)
{ 
    return a.size();
}

int main()
{
    auto future = std::async(simplefunc, "hello world");
    int simple = future.get();
     
    std::cout << "The length is " << simple << std::endl;

    return simple;
}
