/**
 * Sample code to print an address in C++
 * Created on Wed Jan  1 22:26:27 2025
 * 
 * @author: Joseph Stockman @quora.com
 * 
 **/

#include <iostream>


class Foo {};

int main(void) 
{
    Foo myObject; 
    Foo* myObjectPtr = &myObject ;
    
    std::cout << "0x" << std::hex 
		<< reinterpret_cast<intptr_t>(myObjectPtr) << std::endl;
}
