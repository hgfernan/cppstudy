/*
 * Minimal testing of homemade split()
 * Created on Sat Dec 28 20:41:21 2024
 * 
 * @author: hilton
 * 
 **/
#include <stdio.h>   /* fopen(), fclose() */
#include <stdlib.h>  /* calloc(), realloc(), free() */
#include <string.h>  /* strtok() */

#define DIM(x) ( sizeof(x) / sizeof(x[0]) )

size_t 
split(const char* text, const char* sep, char*** fields) 
{
	char*  buf    = (char*)NULL;
	char*  token  = (char*)NULL;
	char**  work  = (char**)NULL;
	size_t result = 0;
	size_t n_field = 0;
	
	buf = (char*)calloc(strlen(text) + 1, sizeof(char));
	strcpy(buf, text);
	
	token = strtok(buf, sep);
    /* TODO if separator not found, leave */
    if (!token) {
		/* TODO report reading problem */
        return 0;
	}
	// printf("1st token '%s'\n", token);
	
	work = (char**)calloc(1024, sizeof(char*));

    /* TODO get all elements */
	while (token) {
		work[n_field] = 
			(char*)calloc(strlen(token) + 1, sizeof(char*));
		strcpy(work[n_field], token);
		
		n_field++;
		token = strtok(NULL, sep);
		// printf("token %ld '%s'\n", n_field, token);
	}

	result = n_field;
	
	free(buf);
	work = (char**)realloc(work, result * sizeof(char*));
	
	*fields = work;
	
	/* Normal function termination */
	return result;
}

int 
find_text(char* const* texts, size_t n_texts, const char* key) 
{
	size_t ind;
	
	/* if there's not where to find, return nothing was found */
	if ((NULL == texts) || (0 == n_texts)) {
		return -1;
	}
	
	/* if there's not what to find, return nothing was found */
	if ((NULL == texts) || (0 == n_texts)) {
		return -1;
	}
	
	for (ind = 0; ind < n_texts; ind++) {
		if (!strcmp(texts[ind], key)) {
			return ind;
		}
	}
	
	return -1;
}

char* 
deblank(const char* input) 
{
	int start = 0;
	int stop = 0;
	size_t length = 0;
	size_t full_length = 0;
	char* result = (char*)NULL;
	
	if (!input) return NULL;
	
	full_length = strlen(input);
	if (0 == full_length) {
		return (char*)calloc(1, sizeof(char*));
	}
	
	for (start = 0; *(input + start) == ' '; start++)
		;
	
	for (stop = full_length - 1; stop > 0; stop--) {
		if (*(input + stop) != ' ') break;
	} 
	
	length = full_length - start + 1; 
	length -= full_length - stop;
	
	result = (char*)calloc(length + 1, sizeof(char));
	strncpy(result, input + start, length);
	
	/* Normal function termination */
	return result;
}

void
test_split(int argc, char* const* argv) 
{
	int rv;	
	int ind_str;	
	char** fields = (char**)NULL;
	size_t errors = 0;
	size_t ind;
	size_t n_fields = 0;
	
	for (ind_str = 0; ind_str < argc; ind_str++) {
		errors = 0;
		
		n_fields = split(argv[ind_str], ",", &fields);
		
		printf("Found %lu fields in '%s'\n", n_fields, argv[ind_str]);
	
		if (0 == n_fields) {
			continue;
		}
	
		for (ind = 0; ind < n_fields; ind++) {
			printf("field %lu, text '%s'\n", ind, fields[ind]);
			
			rv = find_text(fields, n_fields, fields[ind]);
			
			if (-1 == rv) {
				errors++;
					
				printf("field %lu ('%s') was not found\n", 
					ind, fields[ind]);
					
				continue;
			}
			
			if ((size_t)rv != ind) {
				errors++;
				printf("find_text() returned %d for field %lu\n", 
					  rv, ind);
			}
		}
	
		if (0 == errors) {
			printf("All %lu fields of '%s' were found correctly\n\n", 
				n_fields, argv[ind_str]);
		}
		
		for (ind = 0; ind < n_fields; ind++) {
			free(fields[ind]);
		}
		free(fields);
	}
	
	/* normal function termination */
	return;
}

int main(int argc, char* const* argv) 
{
//	int rv;
//	char buf1[] = "Time,Open,High,Low,Close";
	char* strings[] = {"", " a", "b ", " ab ", "a", "  ac   "};
//	char** fields = (char**)NULL;
//	size_t errors = 0;
	size_t ind;
//	size_t n_fields = 0;
	
	test_split(argc, argv);
	
/*
	n_fields = split(buf1, ",", &fields);
	
	printf("Found %lu fields\n", n_fields);
	
	if (0 == n_fields) {
		return 1;
	}
	
	for (ind = 0; ind < n_fields; ind++) {
		printf("field %lu, text '%s'\n", ind, fields[ind]);
		
		rv = find_text(fields, n_fields, fields[ind]);
		
		if (-1 == rv) {
			errors++;
				
			printf("field %lu ('%s') was not found\n", 
				ind, fields[ind]);
				
			continue;
		}
		
		if ((size_t)rv != ind) {
			errors++;
			printf("find_text() returned %d for field %lu\n", 
				  rv, ind);
		}
	}
*/	
	for (ind = 0; ind < DIM(strings); ind++) {
		printf("%2lu original '%s' ", 
			ind, strings[ind]);
		
		char* deb = deblank(strings[ind]);
		
		printf("deblanked '%s'\n", deb);
			
		free(deb);
	}
	
	// Normal function termination 
	return 0;
}
