/**
 * Testbed for the \c insertion_op.hpp library
 * Created on Tue May  7 20:06:01 2024
 *
 * @author: user
 *
 */

 /// \todo transform the tests to the Google Test library

/// The include file below are not necessary, but only documentary

#include<iostream>
#include<map>
#include<list>
#include<vector>
#include<unordered_map>

#include "insertion_op.hpp"

 int main()
 {
     std::cout << "\nLists" << std::endl;

     std::list<int> l{0, 1, 2, 3};
     std::cout << "l: " << l << std::endl;

     std::cout << "\nMaps" << std::endl;

     std::map<std::string, std::size_t>
        m{{"d", 4}, {"e", 5}, {"f", 6}, {"g", 7}};
     std::cout << "m: " << m << std::endl;

     std::cout << "\nUnordered maps" << std::endl;

     std::unordered_map<std::string, std::size_t>
        u{{"h", 8}, {"i", 9}, {"j", 10}, {"k", 11}};
     std::cout << "u: " << m << std::endl;

     std::cout << "\nVectors" << std::endl;

     std::vector<int> v{4, 5, 6, 7};
     std::cout << "v: " << v << std::endl;

     /// Normal function termination
     return 0;
 }
