/*
 * Created on Sat Jan 21 17:51:41 2023
 * @author: Alex B form StackOverflow.com
 * From: 
 * * https://stackoverflow.com/questions/7686939/c-simple-return-value-from-stdthread 
 */
#include <iostream> // std::cout, std::endl
#include <thread>   // std::async()
#include <future>   // std::future<T>

int func() { return 1; }

int main() 
{
    std::future<int> ret = std::async(func);
    int i = ret.get();

    std::cout << "Future return value " << i << std::endl;

    return 0;
}
