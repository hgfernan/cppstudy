/**
 * Minimal use of boost/timer library
 * Created on Mon Apr 15 13:57:22 2024
 * 
 * @author: @aly
 * From:
 * - https://stackoverflow.com/questions/15092504/how-to-time-a-function-in-milliseconds-without-boosttimer
 *
 **/ 

///
///
/// DEPRECATED 
///

#include <iostream>
#include <boost/timer.hpp>

int main()
{
	boost::timer t;
	// <some stuff>
	std::cout << t.elapsed() << std::endl;
	
	// Normal function termination
	return 0;
}
