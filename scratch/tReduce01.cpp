/**
 * C++ reduce sample code, nontrivial, but not too complex
 * Created on Wed Apr 24 11:26:23 2024
 * @author: user
 * From:
 * - https://dev.to/sandordargo/the-big-stl-algorithms-tutorial-reduce-operations-3f1m
 **/

#include <vector>
#include <string>
#include <numeric> // reduce(), accumulate()
#include <iostream>

int main()
{
    std::vector<double> dv{0.333333, 0.333333, 0.333333};
    std::vector<std::string> sv{"aaa", "bbb", "ccc"};

    auto dv_sum = std::reduce(dv.cbegin(), dv.cend());
    std::cout << "Sum of double vector:\n" << dv_sum << std::endl;

    auto sv_sum = std::reduce(sv.cbegin(), sv.cend());
    std::cout << "Sum of string vector:\n" << sv_sum << std::endl;

    sv_sum = std::reduce(sv.cbegin(), sv.cend(),
                         std::string(),
                         [] (std::string previousResult, std::string item){
                             if (previousResult.empty())
                                return item;
                             return previousResult + ", " + item;
                         });
    std::cout << "Sum of string vector, with comma:\n"
              << sv_sum << std::endl;

    /// Normal function termination
    return 0;
}




