/** 
 * How to use \c boost \c program_options library
 * Testbed for \c OrderBench
 * Created on Tue Apr 16 08:46:00 2024
 * 
 * @author: user
 * From: Luke Shirley & systemcpro
 * - https://stargckoverflow.com/questions/45466939/parsing-command-line-arguments-with-boost-program-options-c
 *
 **/

#include <iostream>
#include <boost/program_options.hpp>
#include <exception>

/// Option long names 
#define OL_INPUT   "input"
#define OL_VERBOSE "verbose"

/// Option short names 
#define OS_INPUT   "i" 
#define OS_VERBOSE "v"

/// Option joined names 
#define OJ_INPUT   OL_INPUT   "," OS_INPUT
#define OJ_VERBOSE OL_VERBOSE "," OS_VERBOSE

/// Option descriptions 
#define OD_INPUT   "Input filename"
#define OD_VERBOSE "Verbose output"

namespace po = boost::program_options;

int main(int argc, char* const* argv){
    try {
        std::string input;
        bool        verbose;
        
        po::options_description desc("Allowed options");
        desc.add_options()
            ("help,h", "produce help message")
            (OJ_INPUT  , po::value<std::string>(), OD_INPUT)
            (OJ_VERBOSE, 
             po::value<bool>()->default_value(false)->implicit_value(true), 
             OD_VERBOSE);

        po::variables_map vm;
        po::store(po::parse_command_line(argc, argv, desc), vm);
        po::notify(vm);

        if (vm.count("help")) {
            std::cout << desc << "\n";
            return 0;
          }

        std::vector<std::string> 
            mandatory_opts{OL_INPUT};
        
        bool missing_option = false;
        for (auto mandatory_opt : mandatory_opts) {
            if (! vm.count(mandatory_opt)) {
                std::cerr << "ERROR Option '" << mandatory_opt 
                          << "' was not informed." << std::endl;
                missing_option = true;
            } 
        }
        
        if (missing_option) {
            std::string msg("ERROR Options like ");
            for (auto mandatory_opt : mandatory_opts) {
                msg += "'" + mandatory_opt + "', ";
            }
            msg = msg.substr(0, msg.length() - 2);
            msg += " should be informed";
            std::cerr << msg << std::endl;
                      
            return 1;
        }
        
        input   = vm[OL_INPUT]  .as<std::string>();
        verbose = vm[OL_VERBOSE].as<bool>();
        
        std::cout << OL_INPUT   ": " << input   << std::endl;
        std::cout << std::boolalpha  << OL_VERBOSE ": " << verbose << std::endl;       
    }
    catch(std::exception& e) {
        std::cerr << "error: " << e.what() << "\n";
        return 2;
    }
    catch(...) {
        std::cerr << "Exception of unknown type!\n";
        return 3;
    }
    
    return 0;
}
