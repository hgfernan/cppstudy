/** 
 * Is ther NAN and INFINITY in this C implementation ?
 * Created on Fri Dec 27 21:20:06 2024
 * 
 * @author: hilton
 * 
 **/

/* #if !defined() */
 
#include <math.h>  /* NAN, INFINITY */
#include <stdio.h> /* puts() */
#include <float.h> /*  DBL_MAX, FLT_MAX   */

int main(void) 
{
	printf("__STDC__         %d\n", __STDC__);
	
#if defined(__STDC_VERSION__)
	printf("__STDC_VERSION__ %ld\n", __STDC_VERSION__);	

#else /* ! defined(__STDC_VERSION__) */
	printf("__STDC_VERSION__ not defined\n");	
	
#endif /* defined(__STDC_VERSION__) */
	
	putc('\n', stdout);
	
#if defined(NAN) 
	puts("NAN is defined");
	
#else /* ! defined(NAN) */
	puts("NAN is not defined");
	
#endif /* defined(NAN) */

#if defined(INFINITY) 
	puts("INFINITY is defined");
	
#else /* ! defined(INFINITY) */
	puts("INFINITY is not defined");
	
#endif /* defined(INFINITY) */
	
	putc('\n', stdout);
	
	printf("DBL_MAX %g\n", DBL_MAX);
	printf("FLT_MAX %g\n", FLT_MAX);

	/* Normal function termination */ 
	return 0;
}
