#include <iostream>
#include <vector>

class C {
public: 
	void print_status() {
		std::cout << "Vector size: " << v.size() << std::endl;
	}
	void add_value(int val) {
		v.push_back(val);
	};
private:
	std::vector<int> v;
};

int main()
{
	C c;
	int v;

	c.print_status();

	v = 10;
	std::cout << "Will add " << v << std::endl;
	c.add_value(v);
	c.print_status();

	v = 22;
	std::cout << "Will add " << v << std::endl;

	v = 10;
	std::cout << "Will add " << v << std::endl;
	c.add_value(v);
	c.print_status();

	return 0;
}
