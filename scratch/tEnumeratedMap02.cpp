/**
 * Testbed for class EnumeratedMap
 * Created on Sat Apr 27 15:42:38 2024
 *
 * @author: user
 *
 */

#include <random> // random_device, default_random_device...
#include <iostream>

#include <gtest/gtest.h>

#include "insertion_op.hpp"
#include "enumerated_map.hpp"


int main()
{
    EnumeratedMap em;

    std::string key;
    for (int ind = 0; ind < 3; ind++) {
        key = "Key" + std::to_string(ind);
        auto rv = em.addPair(key, ind);
        std::cout << std::boolalpha
                  << "result: added " << rv.first
                  << ", uiIntKey " << rv.second
                  << ", key '" << key << "'"
                  << std::endl;
        std::cout << "\tSize " << em.size() << std::endl;
        std::cout << std::boolalpha
                  << "\tIs key '" << key
                  << "' in ? " << em.contains(key)
                  << std::endl
        ;
        std::cout << "keyToSizeT\n\t"
                  << em.keyToSizeT() << "\n"
                  << std::endl
        ;

    }

    std::cout << "keyToSizeT\n\t"
              << em.keyToSizeT()
              << std::endl
    ;

    std::cout << "itemStore:\n\t"
              << em.itemStore()
              << std::endl
    ;

    /// Normal function termination
    return 0;
}
