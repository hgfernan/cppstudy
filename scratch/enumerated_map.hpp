/**
 * Header for class EnumeratedMap
 * Created on Fri Apr 26 22:05:21 2024
 *
 * @author: user
 *
 **/

#ifndef ENUMERATED_MAP_HPP_INCLUDED
#define ENUMERATED_MAP_HPP_INCLUDED

#include <string>
#include <vector>
#include <unordered_map>

/// \todo transform into a template
class EnumeratedMap {
public:
    EnumeratedMap() {}
    std::size_t size() const;
    bool contains(std::string key) const {
        auto it = m_keyToSizeT.find(key);

        return !(it == m_keyToSizeT.end());
    }
    std::pair<bool, size_t>
    addPair (std::string key, std::size_t item);
    std::unordered_map<std::string, std::size_t> keyToSizeT() const {
        return m_keyToSizeT;
    }
    std::unordered_map<std::size_t, std::vector<std::size_t>> itemStore()
    const {
        return m_itemStore;
    }

private:
    std::unordered_map<std::string, std::size_t> m_keyToSizeT;
    std::unordered_map<std::size_t, std::vector<std::size_t>> m_itemStore;
}; /// class EnumeratedMap

#endif // ENUMERATED_MAP_HPP_INCLUDED
