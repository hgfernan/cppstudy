/**
 * Class to map strings to integers in an unordered map
 * Created on Thu Apr 25 17:43:06 2024
 *
 * @author: user
 *
 **/

#include <random>   // random_device, default_random_device...
#include <utility>  // pair template
#include <iostream>
#include <unordered_map>


template<typename T>
std::ostream& operator<<(std::ostream& s, const std::vector<T>& v)
{
    s.put('[');

    if (v.size() > 0) {
		for (std::size_t ind = 0; ind < v.size() - 1; ind++) {
			s << v[ind] << ", ";
		}

		s << v[v.size() - 1];
	}
    return s << "]";
}


template<typename K, typename V>
std::ostream& operator<<(std::ostream& s, const std::unordered_map<K, V>& m)
{
    s.put('{');

    std::size_t nelems = m.size();
    if (nelems > 0) {
        auto it = m.cbegin();
		for (std::size_t elemno = 0; elemno < (nelems - 1); elemno++) {
			s << it->first << ": " << it->second << ", ";
            it++;
		}

        s << it->first << ": " << it->second;
	}
    return s << "}";
}

/// \todo transform into a template
class EnumeratedMap {
public:
    EnumeratedMap() {}
    std::size_t size() const;
    bool contains(std::string key) const {
        auto it = m_keyToSizeT.find(key);

        return !(it == m_keyToSizeT.end());
    }
    std::pair<bool, size_t>
    addPair (std::string key, std::size_t item);
    std::unordered_map<std::string, std::size_t> keyToSizeT() const {
        return m_keyToSizeT;
    }
    std::unordered_map<std::size_t, std::vector<std::size_t>> itemStore()
    const {
        return m_itemStore;
    }

private:
    std::unordered_map<std::string, std::size_t> m_keyToSizeT;
    std::unordered_map<std::size_t, std::vector<std::size_t>> m_itemStore;
}; /// class EnumeratedMap

std::size_t EnumeratedMap::size() const
{
    if (m_keyToSizeT.size() != m_itemStore.size()) {
        std::cerr << "**** INTERNAL ERROR Stores with different sizes ! ***";
    }

    return m_keyToSizeT.size();
}

/** \brief Add a pair of key and item to the enumerated map
 *
 * \param key to the map
 * \param item of data to add to vector store
 * \return pair of boolean to indicate success, and ordinal if successful
 *
 */
std::pair<bool, size_t>
EnumeratedMap::addPair (std::string key, std::size_t recno)
{
    std::size_t uiIntKey;
    auto rFind = m_keyToSizeT.find(key);

    /// If key was not found
    if (m_keyToSizeT.end() == rFind) {
        uiIntKey = m_keyToSizeT.size();

        auto rInsert = m_keyToSizeT.insert({key, uiIntKey});
        if (! rInsert.second) {
            return std::make_pair(false, static_cast<std::size_t>(-1));
        }

        std::vector<std::size_t> item{recno};

        auto rInsert2 = m_itemStore.insert({uiIntKey, item});
        if (! rInsert2.second) {
            return std::make_pair(false, static_cast<std::size_t>(-1));
        }

        return std::make_pair(true, uiIntKey);
    }

    /// Key was found, ordinal will be added to vector store
    uiIntKey = rFind->second;

    auto rFind2 = m_itemStore.find(uiIntKey);
    if (m_itemStore.end() == rFind2) {
        return std::make_pair(false, static_cast<std::size_t>(-1));
    }

    rFind2->second.push_back(recno);

    /// Normal function termination
    return std::make_pair(true, uiIntKey);
}


int main()
{
    EnumeratedMap em;

    std::string key;
    for (int ind = 0; ind < 3; ind++) {
        key = "Key" + std::to_string(ind);
        auto rv = em.addPair(key, ind);
        std::cout << std::boolalpha
                  << "result: added " << rv.first
                  << ", uiIntKey " << rv.second
                  << ", key '" << key << "'"
                  << std::endl;
        std::cout << "\tSize " << em.size() << std::endl;
        std::cout << std::boolalpha
                  << "\tIs key '" << key
                  << "' in ? " << em.contains(key)
                  << std::endl
        ;
        std::cout << "keyToSizeT\n\t"
                  << em.keyToSizeT() << "\n"
                  << std::endl
        ;

    }

    std::cout << "keyToSizeT\n\t"
              << em.keyToSizeT()
              << std::endl
    ;

    std::cout << "itemStore:\n\t"
              << em.itemStore()
              << std::endl
    ;

    /// Normal function termination
    return 0;
}
