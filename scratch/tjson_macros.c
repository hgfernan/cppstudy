/**
 * Test of simple macros to present JSON data
 * Created on Wed Feb  5 21:59:31 2025
 * 
 * @author: hilton
 * 
 */

#include "json_macros.h" 

int main(void) {
	JSON_MACROS_VARS
	
	JM_OPEN_ALL(stdout);
	
		JM_OPEN_STRUCT("parameters", stdout);
		
			JM_OPEN_STRUCT("reading", stdout);
				JM_ADD_PAIR_STR("inp", "", stdout);
			JM_CLOSE_STRUCT(stdout);
		
			JM_OPEN_STRUCT("init", stdout);
				JM_ADD_PAIR_INT("L", 5, stdout);
				JM_ADD_PAIR_INT("n_rows", 10, stdout);
				JM_ADD_PAIR_INT("method", 5, stdout);
			JM_CLOSE_STRUCT(stdout);

			JM_OPEN_STRUCT("fitting", stdout);
				JM_ADD_PAIR_STR("obj", "", stdout);
				JM_ADD_PAIR_STR("inp", "", stdout);
			JM_CLOSE_STRUCT(stdout);
		
			JM_OPEN_STRUCT("summary", stdout);
				JM_ADD_PAIR_STR("obj", "", stdout);
			JM_CLOSE_STRUCT(stdout);

			JM_OPEN_STRUCT("predict", stdout);
				JM_ADD_PAIR_STR("obj", "", stdout);
				JM_ADD_PAIR_STR("inp", "", stdout);
				JM_ADD_PAIR_STR("L", "", stdout);
				JM_ADD_PAIR_STR("xpred", "", stdout);
				JM_ADD_PAIR_STR("amse", "", stdout);
			JM_CLOSE_STRUCT(stdout);
		
		JM_CLOSE_STRUCT(stdout);
	
		JM_OPEN_STRUCT("result", stdout);

			JM_OPEN_STRUCT("fitting", stdout);
				JM_ADD_PAIR_INT("aic", 7, stdout);
				JM_ADD_PAIR_INT("order", 7, stdout);
				
				JM_OPEN_ARRAY("coefficients", stdout);
					JM_ADD_ELMT_DBL(0.372554, stdout);
					JM_ADD_ELMT_DBL(0.197209, stdout);
					JM_ADD_ELMT_DBL(0.0202597, stdout);
					JM_ADD_ELMT_DBL(0.013501, stdout);
					JM_ADD_ELMT_DBL(-0.0148193, stdout);
					JM_ADD_ELMT_DBL(0.0625254, stdout);
					JM_ADD_ELMT_DBL(0.156202, stdout);
				JM_CLOSE_ARRAY(stdout);

				JM_ADD_PAIR_DBL("mean", 17.0624, stdout);
				JM_ADD_PAIR_DBL("sigma2", 0.0949526, stdout);
				
			JM_CLOSE_STRUCT(stdout);
			
			JM_OPEN_STRUCT("predict", stdout);
			
				JM_OPEN_ARRAY("xpred", stdout);
					JM_ADD_ELMT_DBL(17.4489, stdout);
					JM_ADD_ELMT_DBL(17.4853, stdout);
					JM_ADD_ELMT_DBL(17.4255, stdout);
					JM_ADD_ELMT_DBL(17.4464, stdout);
					JM_ADD_ELMT_DBL(17.3939, stdout);
				JM_CLOSE_ARRAY(stdout);
			
				JM_OPEN_ARRAY("amse", stdout);
					JM_ADD_ELMT_DBL(0.304906, stdout);
					JM_ADD_ELMT_DBL(0.325378, stdout);
					JM_ADD_ELMT_DBL(0.341126, stdout);
					JM_ADD_ELMT_DBL(0.347595, stdout);
					JM_ADD_ELMT_DBL(0.351388, stdout);
				JM_CLOSE_ARRAY(stdout);
			
			JM_CLOSE_STRUCT(stdout);
		
		JM_CLOSE_STRUCT(stdout);
	
	JM_CLOSE_ALL(stdout);

/*	
            "xpred": [
                17.4489,
                17.4853,
                17.4255,
                17.4464,
                17.3939
            ],	
 */	
	/* Normal function termination */
	return 0;
}
