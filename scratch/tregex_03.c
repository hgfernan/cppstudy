/**
* find-str.c: find string in file
* Created on Sat Dec 28 23:48:08 2024
* 
* @author: Karunesh Johri
* From:
* -- https://www.softprayog.in/programming/regular-expressions-in-c
*
**/

/*
*     
*   find-str.c: find string in file
*     
*/

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <regex.h>

#define ARRAY_SIZE(array) (sizeof((array)) / sizeof((array)[0]))

char re[128];
char buf[1024];

int
main (int argc, char **argv)
{
  FILE *fp;
  regex_t regex;
  regmatch_t pmatch[3];         // Up to 3 sub-expressions
  regoff_t offset, length;
  int ret;

  if (argc != 3) {
    fprintf (stderr, "Usage: find-str pattern file\n");
    exit (EXIT_FAILURE);
  }

  strcpy (re, argv[1]);

  if ((fp = fopen (argv[2], "r")) == NULL) {
    perror ("fopen");
    exit (EXIT_FAILURE);
  }

  // Extended Regular Expressions, case insensitive search
  ret = regcomp (&regex, re, REG_EXTENDED | REG_ICASE | REG_NEWLINE);
  if (ret) {
    (void) regerror (ret, &regex, buf, sizeof (buf));
    fprintf (stderr, "Error: regcomp: %s\n", buf);
    exit (EXIT_FAILURE);
  }

  printf ("Matches:\n");


  while (fgets (buf, sizeof (buf), fp)) {
    char *s = buf;
    for (int i = 0;; i++) {
	ret = regexec (&regex, s, ARRAY_SIZE (pmatch), pmatch, 0);
      if (ret) {
	if (ret != REG_NOMATCH) {
	  (void) regerror (ret, &regex, buf, sizeof (buf));
	  fprintf (stderr, "Error: regexec: %s\n", buf);
	  exit (EXIT_FAILURE);
	}
	break;
      }
      if (i == 0)
	printf ("\n%s", buf);
      offset = pmatch[0].rm_so + (s - buf);
      length = pmatch[0].rm_eo - pmatch[0].rm_so;
      printf ("#%d:\n", i);
      printf ("offset = %jd; length = %jd\n", (intmax_t) offset,
              (intmax_t) length);
      printf ("substring = \"%.*s\"\n", length, s + pmatch[0].rm_so);

      for (size_t j = 0; j < ARRAY_SIZE(pmatch); j++) {
	if (pmatch[j].rm_so == -1)
	  break;
	offset = pmatch[j].rm_so + (s - buf);
	length = pmatch[j].rm_eo - pmatch[j].rm_so;
	printf ("\toffset = %jd; length = %jd\n", (intmax_t) offset,
	        (intmax_t) length);
	printf ("\tsubstring = \"%.*s\"\n", length, s + pmatch[j].rm_so);
	printf ("\t[%lu] %d %d \n", j, pmatch[j].rm_so, pmatch[j].rm_eo);
      }
      s += pmatch[0].rm_eo;
    }
  }

  // free internal storage fields associated with regex
  regfree (&regex);

  if (fclose (fp)) {
    perror ("fclose");
    exit (EXIT_FAILURE);
  }

  exit (EXIT_SUCCESS);
}
