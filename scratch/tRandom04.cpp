/**
 * Sample code for seed_seq
 * Created on Sat Apr 13 18:54:15 2024
 * 
 * @author: @cppreference
 * From:
 * - https://en.cppreference.com/w/cpp/numeric/random/seed_seq
*/

#include <cstdint>
#include <iostream>
#include <random>
 
int main()
{
    std::seed_seq seq{1, 2, 3, 4, 5};
    std::vector<std::uint32_t> seeds(10);
    seq.generate(seeds.begin(), seeds.end());
    for (std::uint32_t n : seeds) {
        std::cout << n << '\n';
	}
	
	return 0;
}
