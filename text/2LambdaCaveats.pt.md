# Dois cuidados de funções lambda em C++

Funções lambda, uma contribuição ao C++ moderno que talvez Alonzo Church não imaginasse, têm muitas vantagens, mas demandam muitos cuidados em seu uso.

Antes mesmo de escrever outro texto apresentando ideias básicas de funções lambda neste blogue, convém comentar um pouco sobre problemas inesperados que elas podem trazer.

Neste caso, serão comentados dois problemas, que são reproduzidos em trechos de código muito pequenos. Todos foram derivados de uma discussão apresentada em [1].

Antes de detalhá-los, convém comentar a parte comum das listagens `tLambda1.cpp` e `tLambda2.cpp`, discutidas a seguir:

```C++
#include <iostream>
#include <functional>

void PerformOperation(std::function<int()> f)
{
    f();
}
```

Na primeira linha, como é óbvio para qualquer programador C++, é feita a inclusão do arquivo de cabeçalho para biblioteca padrão de entrada e saída de fluxos em C++, chamada `iostream`. Na segunda linha é incluído `functional`, o cabeçalho da biblioteca padrão de C++ para *templates* de classes para objetos de função.

É através dele que é definida a função `PerformOperation()`, que recebe como parâmetro um ponteiro para uma função que retorna um inteiro e não usa parâmetros. Ou seja: `int()`.

No corpo de `PerformOperation()`, essa função é chamada por `f();`

Definido o trecho comum, vamos aos problemas específicos

## Alteração de variáveis locais em outro contexto

A princípio, variáveis locais do escopo onde uma função lambda é definida não são nem mesmo reconhecidas no escopo da função lambda: elas têm que ser passadas através de uma lista especial, chamada de *capture list*, ou "lista de captura", de variáveis locais.

Existem duas formas para isso ser feito: em uma delas essas variáveis são passadas de modo semelhante (mas não igual) à chamada por valor (*call by value*); em outra, são passadas por referência (o usual *call by name* de outras linguagens de programação).

Isto cria efeitos um tanto inesperados.

Vejamos no trecho de código a seguir, que deve ser acrescentado ao trecho comum, para se compilado, formando o fonte `tLambda1.cpp`

```C++
int main()
{
    int a = 10;

    auto func = [&]() {
        a++;
        return 0;
    };

    PerformOperation(func);

    std::cout << "After PerformOperation(), a == " << a << std::endl;

    return 0;
}
```

A função `func()` apenas faz incrementar o valor da variável `a`, que é definida localmente em `main()` com o valor 10.

(A palavra reservada `auto` -- introduzida no C++11 -- pede ao compilador C++ que deduza o tipo da expressão. No caso, é fácil ver que ele será `std::function<int()>`, pois é isto o que espera a função `PerformOperation()`.

A execução do código executável gerado a partir de `tLambda1.cpp` tem a saída:

```console output
After PerformOperation(), a == 11
```

Ou seja: uma função definida fora do escopo de `main()` teve o poder de causar a alteração do valor de uma variável local deste escopo.

Considerando que variáveis locais são consideradas exclusivas de um certo escopo, o que garante que outros trechos do programa não vão alterá-las, isto traz para este tipo de chamada o mesmo perigo que se considera que variáveis globais tragam para a programação. Ou seja: alteração em qualquer ponto do programa.

## Uso de valores atribuídos no ponto da definição

O próximo comportamento inesperado de funções lambda em C++, quanto a variáveis passadas pela lista de captura é mostrado no fonte `tLambda2.cpp`, cujo trecho específico é

```C++
int main()
{
    int a = 10;

    auto func = [=]() {
        std::cout << "Inside func(), a == " << a << std::endl;
        return 0;
    };

    std::cout << "Before 1st call of PerformOperation(), a == " << a << std::endl;
    PerformOperation(func);

    a++;
    std::cout << "Before 2nd call of PerformOperation(), a == " << a << std::endl;
    PerformOperation(func);

    return 0;
}
```

Neste código, bastante semelhante a `tLambda1.cpp` visto no item anterior, `func()` não altera mais o valor de `a`, que agora é recebido por valor pela lista de captura. Em vez de alterar, `func()` apenas imprime o valor de `a`, que de novo recebe na inicialização.

O bloco da primeira chamada de `PerformOperation()` gera as linhas de saída

```console output
Before 1st call of PerformOperation(), a == 10
Inside func(), a == 10
```

O que mostra que o valor de a foi corretamente impresso por `PerformOperation()`.

Mas antes da segunda chamada de `PerformOperation()` o valor de `a` é incrementadopela expressão `a++;`

Agora a segunda chamada de `PerformOperation()` gera

```console output
Before 2nd call of PerformOperation(), a == 11
Inside func(), a == 10
```

Ou seja: apesar do valor de `a` ter sido alterado, ele não foi comunicado à função lambda `func()`, que guardou o valor original de `a`, definido antes de sua definição.

Deste modo, contrário ao que poderia supor, o comportamento de uma variável local em lista de captura sem referência não é o mesmo daquele esperado por chamada por valor de uma função convencional em C ou C++.

Apenas para registrar ideias, segue o fonte completo de `tLambda3.cpp`, que usa passagem por valor de uma variável inteira para uma função lambda:

```C++
#include <iostream>
#include <functional>

void PerformOperation(std::function<int(int)> f, int a)
{
    f(a);
}

int main()
{
    int a = 10;

    auto func = [](int a) {
        std::cout << "Inside func(), a == " << a << std::endl;
        return 0;
    };

    std::cout << "Before 1st call of PerformOperation(), a == " << a << std::endl;
    PerformOperation(func, a);

    a++;
    std::cout << "Before 2nd call of PerformOperation(), a == " << a << std::endl;
    PerformOperation(func, a);

    return 0;
}
```

Neste caso, a saída é

```console output
Before 1st call of PerformOperation(), a == 10
Inside func(), a == 10
Before 2nd call of PerformOperation(), a == 11
Inside func(), a == 11
```

mostra que a variável local passada por valor tem seu valor consultado antes da chamada da função. Ou seja: a chamada por valor de uma função lambda tem o mesmo comportamento de uma função convencional. A diferença que deve ser observada com cuidado é na passagem de variáveis por lista de captura, que pode trazer comportamentos bastante diferentes daqueles esperados para passagem de parâmetros de variáveis.
