# Objetivos da série e configuração

A linguagem de programação C++ está em constante evolução desde seu início. Começou com a meta complexa de ser C com classes, mas rapidamente C++ se tornou uma linguagem diferente do C original, com o qual ainda mantém a meta de ser "tão compatível com C quanto possível, mas não mais do que isso".

O criador da linguagem, Bjarne Stroustrup, lançou pelo menos duas versões da linguagem baseado na edição de seu livro *The C++ Programming Language*, em 1985 e 1989. Mas, diante da popularização e importância da linguagem, em 1998 dois organismos de de definição de padrões ISO e IEC se uniram para propor o padrão C++98. Que foi completado em 2003, com o C++03.

A linguagem esteve em discussão desde então, até 2011, quando foi finalmente lançado o padrão C++11, anteriormente chamado C++0x -- pois o objetivo era lançar uma nova versão antes de 2010.

C++2011 representou uma forte mudança da linguagem. 

De 2011 para cá, tem sido lançadas novas versões a cada três anos: C++14 em 2014, C++17 em 2017, e C++20 em 2020.

C++2020 ainda não foi implementada completamente nos compiladores mais ativos, que são o `g++` do projeto GNU e o `clang` do projeto LLVM, mas já está sendo discutida a próxima versão C++23, que se planeja lançar em 2023.

Por uma razão de estabilidade dos compiladores e da base instalada, muitas empresas ainda se mantém usando C++98 ou C++03. Que aliás, já é bastante complexo.

Contudo, essa versão está sendo tornada código legado, depois de 20 anos de retrocompatibilidade.

Assim, é importante tomar contato com as novas versões, o que estaremos fazendo nestas série de textos, vídeos e códigos fonte.

Em vez de fazer uma abordagem teórica, que muitas vezes gera dúvida ou desinteresse, faremos uma abordagem prática das diversas inovações do C++ frente à versão C++03 em uso, o que fará sucessivas transformações de um pequeno código base, para obter uma maior compreensão do conceito, suficiente para a programação prática.

Começaremos com as *expressões lambda*, quem muitos chamam de *funções lambda*.

Usaremos o compilador g++ 10.2, a versão estável mais recente, que implementa grande parte do padrão C++20.

Como os programas são muito simples, não usaremos nenhuma IDE e nem mantenedores de dependências como *makefiles*, similares ou seus geradores como `qmake` ou `CMake`, mas apenas a linha de comando.

Será usado para os testes um sistema operacional Debian 10.8, mas, por serem simples, os comandos executados são de fácil adaptação para outros sistemas operacionais.

Com isso, estaremos focados na linguagem de programação, como é implementada em um dos compiladores líderes do mercado.

Tanto os códigos-fonte, quanto os textos lidos no vídeo estarão em um repositório no gitlab, em

[https://gitlab.com/hgfernan/cppstudy](https://gitlab.com/hgfernan/cppstudy)
