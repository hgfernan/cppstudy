# Expressões lambda em C++ -- parte 6

O vídeo anterior, que discutiu o programa `tLambda25.cpp` trouxe um problema: para testes das expressões lambda há um trecho literalmente repetitivo em todas expressões.

Por exemplo, para testar a expressão lambda apontada por `all_byvalue`, o trecho de código é: 

```C++
    std::cout << "all_byvalue()" << std::endl;
    all_byvalue();
    tell_change("a", a, a_sav); 
    tell_change("b", b, b_sav);
    tell_change("c", c, c_sav);
    std::cout << std::endl;
    restore(a, a_sav); restore(b, b_sav); restore(c, c_sav); 
```

E para testar a expressão lambda `byvalue_xcept_a`, declarada logo a seguir, o trecho é:

```C++
    std::cout << "\nbyvalue_xcept_a()" << std::endl;
    byvalue_xcept_a();
    tell_change("a", a, a_sav); 
    tell_change("b", b, b_sav);
    tell_change("c", c, c_sav);
    std::cout << std::endl;
    restore(a, a_sav); restore(b, b_sav); restore(c, c_sav); 
```

E assim por diante: para cada teste de expressão lambda em `tLambda25.cpp` há `5` linhas absolutamente iguais:

```C++
    tell_change("a", a, a_sav); 
    tell_change("b", b, b_sav);
    tell_change("c", c, c_sav);
    std::cout << std::endl;
    restore(a, a_sav); restore(b, b_sav); restore(c, c_sav); 
```

E duas linhas que podem ser parametrizadas: uma onde é mostrado o nome da expressão e outra a expressão é invocada.

Por isso, foi feita a proposta inicial de uma função

```C++
void test_lambda(const std::string& name, 
                 std::function<void(void)> lambda) 
```

onde seriam passados apenas o nome da expressão e a própria expressão.

Infelizmente, isto não pode ser praticado, pois as listas de captura pressupõem variáveis locais onde a expressão lambda é definida.

Ou seja: uma expressão lambda definida -- por exemplo -- em `main()` só poderá capturar variáveis locais de `main()`.

Outras variáveis podem ser passadas por escopo. Por exemplo, uma expressão lambda poderá usar variáveis globais -- que não poderão ser capturadas.

Contudo, o objetivo do trabalho é lidar com listas de captura. Assim, foi necessário encontrar outra solução.

Uma solução possível é esta que segue. Ela segue o estilo da programação em C, e com certeza haverá uma solução mais ao estilo do C++ moderno.

Mas fica como primeira tentativa de solução. Aceitamos outras soluções, e voltaremos ao assunto em um próximo vídeo.

A solução foi passar as variáveis que devem ser monitoradas como parâmetros para a função de teste. Para minimizar a lista de parâmetros, foi criada uma estrutura para empacotá-los. 

E para passar as variáveis em si, foram passados seus seus endereços. Assim, a estrutura ficou:

```C++
struct val_sav {
    int    *pa, *pa_sav;
    double *pb, *pb_sav;
    float  *pc, *pc_sav;
};
```

Assim, a assinatura da função ficou 

```C++
void test_lambda(const std::string& name,
                 std::function<void(void)> lambda,
                 val_sav& vs) 
```

Isto é: a função foi passada como uma referência, como é típico de C++ -- referências não estão disponíveis em C.

As variáveis foram definidas e inicializadas como antes no programa `tLambda25.cpp` e um objeto do tipo `val_sav` foi definido e inicializado como a seguir em `main()`:

```C++
    val_sav vs;
    vs.pa     = &a;
    vs.pa_sav = &a_sav;
    vs.pb     = &b;
    vs.pb_sav = &b_sav;
    vs.pc     = &c;
    vs.pc_sav = &c_sav;
```

Em `test_lambda()`, as referências as variáveis foram referidas através de seus ponteiros. Por exemplo, em vez do trecho de 

```C++
    tell_change("a", a, a_sav); 
    tell_change("b", b, b_sav);
    tell_change("c", c, c_sav);
    std::cout << std::endl;
```

de `tLambda25.cpp`, no programa `tLambda25.cpp` foi usado

```C++
    tell_change("a", *vs.pa, *vs.pa_sav);
    tell_change("b", *vs.pb, *vs.pb_sav);
    tell_change("c", *vs.pc, *vs.pc_sav);
    std::cout << std::endl;
```

Naturalmente, `*vs.pa` se refere a `a`. E da mesma forma para os outros elementos da estrutura `vs`.

O programa principal, então ficou

```C++
    test_lambda("all_byvalue()", all_byvalue, vs);
    
    test_lambda("byvalue_xcept_a()", byvalue_xcept_a, vs);

    test_lambda("all_byreference()", all_byreference, vs);

    test_lambda("byreference_xcept_b()", byreference_xcept_b, vs);
    
    test_lambda("byreference_xcept_bc()", byreference_xcept_bc, vs);
```

como desejado.

O programa `tLambda26.cpp` executou da mesma forma que o anterior `tLambda25.cpp`

Foi compilado com as versões estáveis mais recentes do `g++` e do `clang++` com o máximo de nível de verificações e não foi gerado nenhum aviso. 
