# Expressões lambda em C++ -- parte 4

Naturalmente, funções lambda não foram inventadas para serem usadas para escrever `Hello, world !`

Seu uso principal é como parâmetros de outras funções. Veja, por exemplo, o código `tLambda23.cpp`, que mostra um uso um pouco mais realista de uma função lambda, neste caso usada para contar elementos pares em um vetor:

```C++
#include <iostream>
#include <functional>
#include <vector>

int how_many(std::vector<int> v, std::function<bool(int)> f)
{
    int result = 0;

    for (int value : v) {
        result += f(value) ? 1 : 0;
    }

    return result;
}

int main() {
    std::vector<int> v { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
    int total = how_many(v,
                        [](int v) { return (v % 2) == 0;});

    std::cout << "Count of values: " << total << std::endl;

    return 0;
}
```

Ela usa o a *template* `std::function<>` para definir uma elemento semelhante a uma função, que é definido no arquivo de cabeçalho `functional`. Neste caso, usa como parâmetro uma função que recebe um número `int` e retorna um valor lógico, ou `bool`. Isto significa que qualquer função com essa assinatura poderia ser usada.

Também é usada a estrutura genérica de dados `std::vector<>`, que permite alocar uma série de dados, com certas vantagens, sobre os vetores convencionais da linguagem C, usados também em C++. No caso, essa estrutura contém números `int`. Entre chaves `{}` estão os inteiros que fazem sua inicialização.

Na função `how_many()]`, os dois elementos são usados para fazer a contagem de todos elementos do vetor que satisfaçam a propriedade de serem pares. Esta forma do comando `for`, que outras linguagens chamam de `foreach`, permite percorrer todos elementos do vetor sem a necessidade de contagem de índices.

Por último, a forma `?:`, também chamada de operador ternário, permite acrescentar `1` quando a função retornar `true`, e `0` quando retornar `false`.

O fato da função que filtra os números pares no ponto da chamada de `how_many()` permite um código mais compacto e mais fácil de ler. Seria muito fácil trocar o argumento para contar números ímpares, por exemplo.  Bastaria, é claro, usar a comparação 
`(v %2 ) == 1`.
