# Temas futuros

1. Visão geral de funções lambda:

    * Cada um dos elementos `[](){}`;

    * Como funções lambda são chamadas;

    * Atribuição de uma função lambda a uma variável;

    * Passagem de parâmetros;

    * Passagem por valor impede alteração da variável local, passada pela lista de captura;

    * Parâmetros têm precedência sobre nomes iguais na lista de captura:

      ```C++
      int a = 10;

       auto func = [=](int a) {
         std::cout << "Inside func(), a == " << a << std::endl;
         return 0;
      };
      ```

    * Passagem por referência traz problemas inesperados;

    * Formas da lista de captura;

    * Toda expressão lambda retorna um valor ?

    * Expressões lambda geram código mais eficiente do que funções convencionais ?

    * Expressões lambda geram código mais eficiente do que funções *inline* ?

    * Expressões lambda verificam o tipo de seus parâmetros ?

    * Quando passadas como parâmetros, as Expressões lambda têm sua assinatura verificada ?

    * Diferença entre definição de elementos da função (parâmetros ?), e os elementos da chamada (argumentos ?)

    * *It is an expression, not a function ?*

    * *Templates* e expressões lambda

    * Inferência errônea de valor de retorno de expressão lambda.

    * Confirmação de que, depois de uma expressão generalizada 
    `&` ou `=`, os elementos individuais são excessões.

2. Utilidade e significado do declarador `auto`.

3. Parameter not checked in `void PerformOperation(std::function<int()> f)` ?

4. Inicialização de objetos, particularmente vetores

5. Recursos para concorrência: funções e classes

6. Recursos para programação funcional
