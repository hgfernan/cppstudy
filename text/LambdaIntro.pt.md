# Funções lambda em C++

Muito do código C++ em produção nas empresas hoje em dia usa o padrão C++98, também conhecido como
C++03, que já tem perto de 20 anos. *Funções lambda* são um conceito mais recente, tendo sido introduzidas no padrão C++11.

Como muitos conceitos do C++, elas podem trazer complexidades inesperadas. Por isto, nesta introdução ao tema, em vez de uma abordagem mais formal e teórica, vão ser lançadas várias intuições práticas, progressivamente mais complexas, até se atingir uma compreensão suficiente do conceito, que permita ao leitor usar funções lambda em seus programas.

Para evitar textos longos, fora do padrão da Internet, pontos mais específicos serão tratados por textos à parte.

Por último, vários dos textos serão apresentados em vídeos no YouTube.

## Razões para usar funções lambda

A razão para usar funções lambda é que a programação genérica e a programação funcional, hoje muito usadas, exigem pequenos trechos de programação, tão pequenos que muitas vezes não vale a pena criar funções e métodos convencionais para implementá-los.

Além disso, quando se implementa um trecho desses através de função ou método convencional, quando um programador encontra sua chamada, tem que desviar da leitura do código naquele ponto, procurar onde está definida a função, entender o que ela faz, para então retornar ao ponto anterior do código.

Assim, resumindo, a maior vantagem das funções lambda é que permitem criar pequenas funções de modo tão conciso que podem ser implementadas diretamente no trecho de código onde serão usadas.

## A função lambda mais simples

A função lambda mais simples é simplesmente

```C++
[](){};
```

Observe que é terminada por um ponto-e-vírgula, ou `;`

Em termos sintáticos, ela é uma expressão, não uma declaração, como uma função ou método comum do C++.

* O par de colchetes `[]` inicial permite usar variáveis locais no corpo da função lambda. Por isso é chamado de *lista de captura*.

    No caso, nenhuma variável do escopo local está sendo usada;

* O par de parênteses `()` permite passar uma lista de parâmetros para a função lambda, exatamente como em uma função convencional do C++.

    No caso, nenhum parâmetro está sendo passado;

* O par de chaves `{}` é o corpo da função.

    Aparentemente, não há nenhum comando no corpo desta função lambda. Isto será discutido a seguir.

Como se pode ver, uma função lambda não tem nome: apenas lista de captura, parâmetros e corpo de código. Outro nome para uma função lambda é **função anônima**.

O pequeno programa `tLambda5.cpp` mostra um corpo de programa comentado, onde a função mínima é mostrada.

O programa `tLambda6.cpp` pode ser usado para avaliar se uma função lambda assim escrita de fato realiza alguma ação.

```C++
#include <iostream>

int main()
{
    [](){ std::cout << "Hello, world !" << std::endl; };

    return 0;
}
```

Mas, quando executado, não surge a famosa mensagem `Hello, world !` na linha de comando.

Assim, um pouco mais de informação nos leva a acrescentar um novo elemento ao programa. Veja então `tLambda7.cpp`:

```C++
#include <iostream>

int main()
{
    [](){ std::cout << "Hello, world !" << std::endl; }();

    return 0;
}
```

Agora a saída será:

```console output
Hello, world !
```

como esperado.

## Um exemplo mais realista

Naturalmente, funções lambda não foram inventadas para serem usadas para escrever `Hello, world !`

Seu uso principal é como parâmetros de outras funções. Veja, por exemplo, o código `tLambda8.cpp`, que mostra um uso um pouco mais realista de uma função lambda, neste caso usada para contar elementos pares em um vetor:

```C++
#include <iostream>
#include <functional>
#include <vector>

int how_many(std::vector<int> v, std::function<bool(int)> f)
{
    int result = 0;

    for (int value : v) {
        result += f(value) ? 1 : 0;
    }

    return result;
}

int main() {
    std::vector<int> v { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
    int total = how_many(v,
                        [](int v) { return (v % 2) == 0;});

    std::cout << "Count of values: " << total << std::endl;

    return 0;
}
```

Ela usa o a *template* `std::function<>` para definir uma elemento semelhante a uma função, que é definido no arquivo de cabeçalho `functional`. Neste caso, usa como parâmetro uma função que recebe um número `int` e retorna um valor lógico, ou `bool`. Isto significa que qualquer função com essa assinatura poderia ser usada.

Também é usada a estrutura genérica de dados `std::vector<>`, que permite alocar uma série de dados, com certas vantagens, sobre os vetores convencionais da linguagem C, usados também em C++. No caso, essa estrutura contém números `int`. Entre chaves `{}` estão os inteiros que fazem sua inicialização.

Na função `how_many()]`, os dois elementos são usados para fazer a contagem de todos elementos do vetor que satisfaçam a propriedade de serem pares. Esta forma do comando `for`, que outras linguagens chamam de `foreach`, permite percorrer todos elementos do vetor sem a necessidade de contagem de índices.

Por último, a forma `?:`, também chamada de operador ternário, permite acrescentar `1` quando a função retornar `true`, e `0` quando retornar `false`.

O fato da função que filtra os números pares no ponto da chamada de `how_many()` permite um código mais compacto e mais fácil de ler. Seria muito fácil trocar o argumento para contar números ímpares, por exemplo.  Bastaria, é claro, usar a comparação `(v %2 ) == 1`.

## Tipo de uma função lambda

O exemplo de função lambda mais simples permitiu entender que, para acionar uma função lambda, é preciso um par adicional de parênteses, além daqueles que definem seus argumentos.

Esquecendo um pouco dos detalhes da linguagem, podemos pensar em uma função lambda como definindo um endereço de função convencional, como em C.

***To be completed***

## Os parâmetros de uma função lambda

***To be completed***

## A lista de captura

***To be completed***

## O tipo de dados retornado

* `auto` without a name

***To be completed***

## Referências

***To be completed***
