# Expressões lambda em C++ -- parte 1

Muito do código C++ em produção nas empresas hoje em dia usa o padrão C++98, também conhecido como
C++03, que já tem perto de 20 anos. *Funções lambda* são um conceito mais recente, tendo sido introduzidas no padrão C++11.

Aliás, *função lambda* é um termo incorreto, apesar de popular. Formalmente, o correto é *expressão lambda*. A razão dessa diferença vai ficar clara ao longo destas discussões.

Como muitos conceitos do C++, elas podem trazer complexidades inesperadas. Por isto, nesta série de textos de introdução ao tema, em vez de uma abordagem mais formal e teórica, vão ser lançadas várias intuições práticas, progressivamente mais complexas, até se atingir uma compreensão suficiente do conceito, que permita ao leitor usar funções lambda (aham...) em seus programas.

Para evitar textos longos, fora do padrão da Internet, pontos mais específicos serão tratados por textos à parte.

Por último, praticamente todos textos serão apresentados em vídeos no YouTube.

## Razões para usar funções lambda

A razão prática para usar expressões lambda é que a programação genérica e a programação funcional, hoje muito usadas, exigem pequenos trechos de programação, tão pequenos que muitas vezes não vale a pena criar funções e métodos convencionais para implementá-los.

Além disso, quando se implementa um trecho desses através de função ou método convencional, quando um programador encontra sua chamada, tem que desviar da leitura do código naquele ponto, procurar onde está definida a função, entender o que ela faz, para então retornar ao ponto anterior do código.

Assim, resumindo, a maior vantagem das funções lambda é que permitem criar pequenas funções de modo tão conciso que podem ser implementadas diretamente no trecho de código onde serão usadas.

## A função lambda mais simples

A função lambda mais simples é simplesmente

```C++
[](){};
```

Observe que é terminada por um ponto-e-vírgula, ou `;`

Em termos sintáticos, ela é uma expressão, não uma declaração, como uma função ou método comum do C++.

* O par de colchetes `[]` inicial permite usar variáveis locais no corpo da função lambda. Por isso é chamado de *lista de captura*.

    No caso, nenhuma variável do escopo local está sendo usada;

* O par de parênteses `()` permite passar uma lista de parâmetros para a função lambda, exatamente como em uma função convencional do C++.

    No caso, nenhum parâmetro está sendo passado;

* O par de chaves `{}` é o corpo da função.

    Aparentemente, não há nenhum comando no corpo desta expressão lambda. Isto será discutido a seguir.

Como se pode ver, uma expressão lambda não tem nome: apenas lista de captura, parâmetros e corpo de código. Outro nome para uma expressão lambda é **função anônima**.

O pequeno programa `tLambda5.cpp` mostra um corpo de programa comentado, onde a expressão lambda mínima é mostrada.

O programa `tLambda6.cpp` pode ser usado para avaliar se uma expressão lambda assim escrita de fato realiza alguma ação.

```C++
#include <iostream>

int main()
{
    [](){ std::cout << "Hello, world !" << std::endl; };

    return 0;
}
```

Mas, quando executado, não surge a famosa mensagem `Hello, world !` na linha de comando.

Assim, um pouco mais de informação nos leva a acrescentar um novo elemento ao programa. Veja então `tLambda7.cpp`:

```C++
#include <iostream>

int main()
{
    [](){ std::cout << "Hello, world !" << std::endl; }();

    return 0;
}
```

Agora a saída será:

```console output
Hello, world !
```

como esperado.

O que houve ?

O exemplo a seguir pode dar uma explicação para a execução bem sucedida de `tLambda7.cpp`. Veja o fonte `tLambda8.cpp`:

```C++
#include <iostream>

int main()
{
    auto func = [](){ std::cout << "Hello, world !" << std::endl; };

    func();

    return 0;
}
```

Primeiramente ele atribui o corpo da função lambda a uma variável `func`, que é do tipo `auto`.  A expressão `auto` é uma benção trazida para os programadores C++, a partir da versão C++11. Ela permite fazer declarações sem mencionar todos tipos envolvidos que seriam exigidos em versões mais antigas do C++.

A palavra reservada `auto` é um *placeholder type specifier* -- ele deduz os tipos necessários de variáveis, funções e valores de retorno a partir do contexto.

Assim, ela permitiu criar a variável `func` que contém o tipo da expressão lambda. E, finalmente, na linha a seguir, `func` é chamada através da sintaxe `func()`.

Ou seja: em termos muito simples, compreensíveis por um programador C, `func` é um apontador de função, que precisa ser chamado pela sintaxe usual de chamada de funções `func()`.
